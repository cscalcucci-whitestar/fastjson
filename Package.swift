// swift-tools-version:5.2

import PackageDescription

let package = Package(
    name: "FastJSON",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(name: "FastJSON",
                 targets: ["FastJSON"])
    ],
    dependencies: [
        .package(name: "ChristyK", url: "https://cscalcucci-whitestar@bitbucket.org/cscalcucci-whitestar/christy-k.git", from: "1.0.19")
    ],
    targets: [
        /// The C++ Library
        .target(
            name: "FastCJSON",
            exclude: ["include"] // will be fixed with PR-2814
        ),
        .target(
            name: "FastJSONWrapper",
            dependencies: ["FastCJSON"],
            exclude: ["include"]),
        /// The Objective-C++ Library
        .target(
            name: "FastJSON",
            dependencies: ["FastJSONWrapper", "ChristyK"])
    ],
    cLanguageStandard: .c11,
    cxxLanguageStandard: .cxx14
)

/*
 ,
 cxxSettings: [.unsafeFlags(["-D","BOOST_ALL_NO_LIB=1","-D","DATE_TIME_INLINE","-D","NDEBUG","-m64"])]
 */
