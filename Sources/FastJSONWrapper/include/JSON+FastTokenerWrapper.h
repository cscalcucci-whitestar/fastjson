//
//  JSONFastTokenerWrapper.hpp
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FastCStringReader.h"

@interface JSONFastTokenerWrapper : NSObject
- (JSONFastTokenerWrapper*) init:(NSString *)s;
- (void) back;
- (bool) end;
- (char) next;
- (char) nextFast;
- (NSString*) next:(const int) n;
- (char) nextClean;
- (NSString*) nextString:(const char) quote;
- (NSString*) nextValue;
- (NSString*) getInitial;

@end
