//
//  FastCJSONEncoderWrapper.h
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/16/20.
//  Copyright © 2020 Society. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FastCJSONEncoderWrapper : NSObject
- (FastCJSONEncoderWrapper*) init;
- (NSString*) quote:(NSString*) s;
@end
