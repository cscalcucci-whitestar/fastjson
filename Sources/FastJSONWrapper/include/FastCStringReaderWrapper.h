//
//  FastCStringReaderWrapper.h
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FastCStringReaderWrapper : NSObject
- (FastCStringReaderWrapper*) init:(NSString *)s;
- (int) read;
- (long) skip:(long long) ns;
- (bool) ready;
- (bool) markSupported;
- (void) setMark:(int) readAheadLimit;
- (void) reset;

@end
