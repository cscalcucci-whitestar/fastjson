//
//  JSONFastTokenerWrapper.mm
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//


#import <string>

#import "JSON+FastTokener.h"
#import "JSON+FastTokenerWrapper.h"

@interface JSONFastTokenerWrapper () {
    JSONFastTokener *tokener;
    NSString *initialValue;
}
@end

@implementation JSONFastTokenerWrapper

    - (JSONFastTokenerWrapper*) init:(NSString *)s {
        initialValue = s;
        tokener = new JSONFastTokener([s UTF8String]);
        return self;
    }

    - (void)dealloc {
        delete (tokener);
    }

    - (NSString*) getInitial {
        return initialValue;
    }

    - (void) back {
        tokener->back();
    }

    - (bool) end {
        return tokener->end();
    }

    - (char) next {
        return tokener->next();
    }

    - (char) nextFast {
        return tokener->nextFast();
    }

    - (NSString*) next:(const int) n {
        return [NSString stringWithUTF8String:(tokener->next(n)).c_str()];
    }

    - (char) nextClean {
        return tokener->nextClean();
    }

    - (NSString*) nextString:(const char) quote {
        return [NSString stringWithUTF8String:(tokener->nextString(quote)).c_str()];
    }

    - (NSString*) nextValue {
        return [NSString stringWithUTF8String:(tokener->nextValue()).c_str()];
    }

@end
