//
//  FastCStringReaderWrapper.mm
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//

#import "FastCStringReaderWrapper.h"
#import "FastCStringReader.h"
#import <string>

@interface FastCStringReaderWrapper () {
  FastCStringReader* reader;
}
@end

@implementation FastCStringReaderWrapper

    - (FastCStringReaderWrapper*) init:(NSString *)s {
        reader = new FastCStringReader([s UTF8String]);
        return self;
    }

    - (void)dealloc {
        delete (reader);
    }

    - (int) read {
        return reader->read();

        /*try {
        }
        catch (...) {
            NSLog(@"Error");
        }*/
    }

    - (long) skip:(long long) ns {
        return reader->skip(ns);
    }

    - (bool) ready {
        return reader->ready();
    }

    - (bool) markSupported {
        return reader->markSupported();
    }

    - (void) setMark:(int) readAheadLimit {
        reader->setMark(readAheadLimit);
    }

    - (void) reset {
        reader->reset();
    }

@end
