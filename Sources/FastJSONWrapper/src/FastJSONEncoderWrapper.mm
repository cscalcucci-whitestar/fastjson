//
//  FastCJSONEncoderWrapper.mm
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/16/20.
//  Copyright © 2020 Society. All rights reserved.
//

#import "FastCJSONEncoder.h"
#import "FastCJSONEncoderWrapper.h"
#import <string>

@interface FastCJSONEncoderWrapper () {
  FastCJSONEncoder* encoder;
}
@end

@implementation FastCJSONEncoderWrapper

    - (FastCJSONEncoderWrapper*) init {
        encoder = new FastCJSONEncoder();
        return self;
    }

    - (void)dealloc {
        delete (encoder);
    }

    - (NSString*) quote:(NSString*) s {
        return [NSString stringWithUTF8String:(encoder->quote([s UTF8String])).c_str()];
    }

@end
