//
//  FastCJSONEncoder.h
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/16/20.
//  Copyright © 2020 Society. All rights reserved.
//

#ifndef JSON_Encoder_hpp
#define JSON_Encoder_hpp

#if defined __cplusplus

#include <stdio.h>
#include <string>

class FastCJSONEncoder final {

public:

    /**
     * Creates a new fast JSON encoder.
     */
    FastCJSONEncoder();

    /**
     * Creates a 'JSON' ready quoted string from the input.
     *
     * @param s The string to be JSONified.
     */
    std::string quote(std::string s);
};

#endif /* __cplusplus */
#endif /* JSON_Encoder_hpp */
