//
//  JSON+FastTokener.h
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//


#ifndef JSON_FastTokener_hpp
#define JSON_FastTokener_hpp
#if defined __cplusplus

#include <stdio.h>
#include <string>
#include "FastCStringReader.h"

class JSONFastTokener final {

private:
    bool eof = false;
    bool usePrevious = false;
    long long character = 0;
    long long index = 0;
    long long line = 0;
    char previous = L'\0';
    char lengthCharBuffer[16];

public:
    FastCStringReader *reader;
    
    ~JSONFastTokener() {
        delete (reader);
    }

    JSONFastTokener(std::string s);

    /**
     * Back up one character. This provides a sort of lookahead capability, so
     * that you can test for a digit or letter before attempting to parse the
     * next number or identifier.
     */
    void back();

    bool end();

    /**
     * Get the next character in the source string.
     *
     * @return The next character, or 0 if past the end of the source string.
     */
    char next();

    /**
     * Get the next character in the source string.
     *
     * @return The next character, or 0 if past the end of the source string.
     */
    char nextFast();

    /**
     * Get the next n characters.
     *
     * @param n The number of characters to take.
     * @return A string of n characters.
     * @throws JSONException Substring bounds error if there are not n
     * characters remaining in the source string.
     */
    std::string next(int n);

    /**
     * Get the next char in the string, skipping whitespace.
     *
     * @throws JSONException something went wrong
     * @return A character, or 0 if there are no more characters.
     */
    char nextClean();

    /**
     * Return the characters up to the next close quote character. Backslash
     * processing is done. The formal JSON format does not allow strings in
     * single quotes, but an implementation is allowed to accept them.
     *
     * @param quote The quoting character, either " or '
     * @return A String.
     * @throws JSONException Unterminated string.
     */
    std::string nextString(char quote);

    /**
     * Get the next value. The value can be a Boolean, Double, Integer,
     * JSONArray, JSONObject, Long, or String, or the JSONObject.NULL object.
     *
     * @throws JSONException If syntax error.
     *
     * @return An object.
     */
    std::string nextValue();

};

#endif /* __cplusplus */
#endif /* JSON_FastTokener_hpp */
