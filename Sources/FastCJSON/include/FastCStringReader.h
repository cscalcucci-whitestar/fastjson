//
//  FastCStringReader.h
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//

#ifndef FastCStringReader_hpp
#define FastCStringReader_hpp
#if defined __cplusplus

#include <stdio.h>
#include <string>

class FastCStringReader final {

private:
    int length;
    int next = 0;
    int mark = 0;

public:
    std::string str;

    /**
    * Creates a new string reader.
    *
    * @param s String providing the character stream.
    */
    FastCStringReader(std::string s);
    /*FastCStringReader(std::string s) {
        this->str = s;
        this->length = s.length();
    }*/

    /**
     * Reads a single character.
     *
     * @return The character read, or -1 if the end of the stream has been
     * reached
     *
     * @exception IOException If an I/O error occurs
     */
    int read();

    /**
     * Skips the specified number of characters in the stream. Returns the
     * number of characters that were skipped.
     *
     *
     * The 'ns' parameter may be negative, even though the
     * 'skip' method of the Reader superclass throws an
     * exception in this case. Negative values of 'ns' cause the
     * stream to skip backwards. Negative return values indicate a skip
     * backwards. It is not possible to skip backwards past the beginning of the
     * string.
     *
     *
     * If the entire string has been read or skipped, then this method has no
     * effect and always returns 0.
     *
     * @param ns The number to skip
     * @return  the number of characters that were skipped
     * @exception IOException If an I/O error occurs
     */
    long skip(long long ns);

    /**
     * Tells whether this stream is ready to be read.
     *
     * @return True if the next read() is guaranteed not to block for input
     *
     * @exception IOException If the stream is closed
     */
    bool ready();

    /**
     * Tells whether this stream supports the mark() operation, which it does.
     *
     * @return true if supported
     */
    bool markSupported();

    /**
     * Marks the present position in the stream. Subsequent calls to reset()
     * will reposition the stream to this point.
     *
     * @param readAheadLimit Limit on the number of characters that may be read
     * while still preserving the mark. Because the stream's input comes from a
     * string, there is no actual limit, so this argument must not be negative,
     * but is otherwise ignored.
     *
     * @exception IllegalArgumentException If {readAheadLimit < 0}
     * @exception IOException If an I/O error occurs
     */
    void setMark(int readAheadLimit);

    /**
     * Resets the stream to the most recent mark, or to the beginning of the
     * string if it has never been marked.
     *
     * @exception IOException If an I/O error occurs
     */
    void reset();

};

#endif /* __cplusplus */
#endif /* FastCStringReader_hpp */
