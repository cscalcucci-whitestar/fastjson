//
//  FastCStringReader.mm
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//

#include <math.h>
#include <stdexcept>
#include <vector>

#include "FastCStringReader.h"

/**
 * Creates a new string reader.
 *
 * @param s String providing the character stream.
 */
FastCStringReader::FastCStringReader(std::string s) : str(s), length(s.length()) {
}

/**
 * Reads a single character.
 *
 * @return The character read, or -1 if the end of the stream has been
 * reached
 *
 * @exception IOException If an I/O error occurs
 */
int FastCStringReader::read() {
    if (next >= length) {
        return -1;
    }
    return str.at(next++);
}

/**
 * Skips the specified number of characters in the stream. Returns the
 * number of characters that were skipped.
 *
 *
 * The 'ns' parameter may be negative, even though the
 * 'skip' method of the Reader superclass throws an
 * exception in this case. Negative values of 'ns' cause the
 * stream to skip backwards. Negative return values indicate a skip
 * backwards. It is not possible to skip backwards past the beginning of the
 * string.
 *
 *
 * If the entire string has been read or skipped, then this method has no
 * effect and always returns 0.
 *
 * @param ns The number to skip
 * @return  the number of characters that were skipped
 * @exception IOException If an I/O error occurs
 */
long FastCStringReader::skip(long long ns) {
    if (next >= length) {
        return 0;
    }
    // Bound skip by beginning and end of the source
    long long n = fmin(length - next, ns);
    n = fmax(-next, n);
    next += n;
    return n;
}

/**
 * Tells whether this stream is ready to be read.
 *
 * @return True if the next read() is guaranteed not to block for input
 *
 * @exception IOException If the stream is closed
 */
bool FastCStringReader::ready() {
    return true;
}

/**
 * Tells whether this stream supports the mark() operation, which it does.
 *
 * @return true if supported
 */
bool FastCStringReader::markSupported() {
    return true;
}

/**
 * Marks the present position in the stream. Subsequent calls to reset()
 * will reposition the stream to this point.
 *
 * @param readAheadLimit Limit on the number of characters that may be read
 * while still preserving the mark. Because the stream's input comes from a
 * string, there is no actual limit, so this argument must not be negative,
 * but is otherwise ignored.
 *
 * @exception IllegalArgumentException If {readAheadLimit < 0}
 * @exception IOException If an I/O error occurs
 */
void FastCStringReader::setMark(int readAheadLimit) {
    mark = next;
}

/**
 * Resets the stream to the most recent mark, or to the beginning of the
 * string if it has never been marked.
 *
 * @exception IOException If an I/O error occurs
 */
void FastCStringReader::reset() {
    next = mark;
}
