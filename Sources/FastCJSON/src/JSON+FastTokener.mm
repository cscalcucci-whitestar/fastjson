//
//  JSON+FastTokener.cpp
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/11/20.
//  Copyright © 2020 Society. All rights reserved.
//

#include "JSON+FastTokener.h"
#include <cstddef>
#include <iostream>
//#include <boost/algorithm/string.hpp>

//FastStringReader::FastStringReader(const std::string &s) : str(s), length(s.length()) {
JSONFastTokener::JSONFastTokener(std::string s) : reader(new FastCStringReader(s)) {
    this->eof = false;
    this->usePrevious = false;
    this->previous = 0;
    this->index = 0;
    this->character = 1;
    this->line = 1;
}

void JSONFastTokener::back() {
    if (this->usePrevious || this->index <= 0) {
        throw L"Stepping back two steps is not supported";
    }
    this->index -= 1;
    this->character -= 1;
    this->usePrevious = true;
    this->eof = false;
}

bool JSONFastTokener::end() {
    return this->eof && !this->usePrevious;
}

char JSONFastTokener::next() {
    int c;

    if (this->usePrevious) {
        this->usePrevious = false;
        c = this->previous;
    } else {
        try {
            c = this->reader->read();
        } catch(...) {
            std::cout << L"^^^Reader.next() threw an exception";
            throw L"Reader.next() threw an exception";
        }

        if ((c == -1) || (c == 0)) { // End of stream
            this->eof = true;
            c = 0;
        }
    }
    this->index += 1;
    if (this->previous == L'\r') {
        this->line += 1;
        this->character = c == L'\n' ? 0 : 1;
    } else if (c == L'\n') {
        this->line += 1;
        this->character = 0;
    } else {
        this->character += 1;
    }
    this->previous = c;
    return this->previous;
}

char JSONFastTokener::nextFast() {
    int c;

    try {
        c = this->reader->read();
    } catch(...) {
        std::cout << L"^^^Reader.nextFast() threw an exception";
        throw L"Reader.nextFast() threw an exception";
    }

    if ((c == -1) || (c == 0)) { // End of stream
        this->eof = true;
        c = 0;
    }
    this->index += 1;
    this->character += 1;
    this->previous = c;
    return this->previous;
}

std::string JSONFastTokener::next(int n) {
    if (n == 0) {
        return "";
    }

    char chars[n];
    int pos = 0;

    while (pos < n) {
        chars[pos] = this->next();
        if (this->end()) {
            std::cout << L"^^^Reader.next() - Substring bounds error";
            throw L"Substring bounds error";
        }
        pos += 1;
    }
    return std::string(chars);
}

char JSONFastTokener::nextClean() {

    for (;;) {
        char c = this->next();
        if (c == 0 || c > L' ') {
            return c;
        }
    }
}

std::string JSONFastTokener::nextString(char quote) {
    char c;

    std::string sb;

    for (;;) {
        c = this->next();

        switch(c) {
            case 0:
                std::cout << L"^^^Unterminated String";
                throw L"Unterminated String";
            case '\n':
                std::cout << L"^^^Unterminated String";
                throw L"Unterminated String";
            case '\r':
                std::cout << L"^^^Unterminated String";
                throw L"Unterminated String";
            case '\\':
                c = this->next();

                switch (c){
                    case 'b':
                        sb += '\b';
                        break;
                    case 't':
                        sb += '\t';
                        break;
                    case 'n':
                        sb += '\n';
                        break;
                    case 'f':
                        sb += '\f';
                        break;
                    case 'r':
                        sb += '\r';
                        break;
                    case 'u':
                        sb += (char)std::stoi(this->next(4), nullptr, 16);
                        break;
                    case '"':
                    case '\'':
                    case '\\':
                    case '/':
                        sb += c;
                        break;
                    default:
                        std::cout << L"^^^Illegal Escape";
                        throw L"Illegal escape";
                }
                break;
            default:
                if (c == quote) {
                    return sb;
                }
                sb += c;
                break;
        }
    }
}

// trim from left
inline std::string& ltrim(std::string& s, const char* t = " \t\n\r\f\v")
{
    s.erase(0, s.find_first_not_of(t));
    return s;
}

// trim from right
inline std::string& rtrim(std::string& s, const char* t = " \t\n\r\f\v")
{
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}

// trim from left & right
inline std::string& trim(std::string& s, const char* t = " \t\n\r\f\v")
{
    return ltrim(rtrim(s, t), t);
}

std::string JSONFastTokener::nextValue() {
    char c = this->nextClean();

    switch (c) {
        case '"':
            return this->nextString(c);
        case '{':
            this->back();
            return "JSONObject";
        case '[':
            this->back();
            return "JSONArray";
    }

    /*
     * Handle unquoted text. This could be the values true, false, or
     * null, or it can be a number. An implementation (such as this one)
     * is allowed to also accept non-standard forms.
     *
     * Accumulate characters until we reach the end of the text or a
     * formatting character.
     */
    std::string sb;
    std::string finder = ",:]}/\\\"[{;=#";
    // FIXME: This might be wrong
    while (c >= ' ' && finder.find(c) == std::string::npos) {
        sb += c;
        c = this->next();
    }

    this->back();

    //boost::trim(sb);
    trim(sb);

    if (sb.length() == 0) {
        std::cout << L"^^^Missing Value";
        throw L"Missing value!";
    }
    return "JSONObject+" + sb;
}


