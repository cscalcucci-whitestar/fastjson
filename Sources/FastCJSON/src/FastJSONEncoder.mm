//
//  FastCJSONEncoder.mm
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/16/20.
//  Copyright © 2020 Society. All rights reserved.
//

#include "FastCJSONEncoder.h"

FastCJSONEncoder::FastCJSONEncoder() {}

/**
 * Creates a 'JSON' ready quoted string from the input.
 *
 * @param s The string to be JSONified.
 */
std::string FastCJSONEncoder::quote(std::string s) {
    std::string sb;

    int len = s.length();

    if (len == 0) {
        sb += '"';
        sb += '"';
        return sb;
    }

    char b;
    char c = 0;
    std::string hhhh;

    sb += '"';

    for (int i = 0; i < len; i++) {
        b = c;
        c = s.at(i);

        switch (c) {
            case '\\':
            case '"':
                sb += '\\';
                sb += c;
                break;
            case '/':
                if (b == '<') {
                    sb += '\\';
                }
                sb += c;
                break;
            case '\b':
                sb += "\\b";
                break;
            case '\t':
                sb += "\\t";
                break;
            case '\n':
                sb += "\\n";
                break;
            case '\f':
                sb += "\\f";
                break;
            case '\r':
                sb += "\\r";
                break;
            default:
                /*if (c < ' ' || (c >= '\u0080' && c < '\u00a0') || (c >= '\u2000' && c < '\u2100')) {

                } else {
                    sb += c;
                }*/
                sb += c;
                break;
        }
    }
    sb += '"';
    return sb;
}
