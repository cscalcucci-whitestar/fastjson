//
//  JSONCodingKey.swift
//  WhiteStarJSON
//
//  Created by Chris Scalcucci on 9/3/20.
//  Copyright © 2020 WhiteStar. All rights reserved.
//

import Foundation

public protocol JSONCodingKey : Hashable, CustomStringConvertible {
    var rawValue : String { get }
}

extension JSONCodingKey {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(rawValue)
    }

    public var description : String {
        return rawValue
    }
}
