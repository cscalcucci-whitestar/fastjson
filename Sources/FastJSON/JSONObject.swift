//
//  JSONObject.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 7/27/20.
//  Copyright © 2020 Society. All rights reserved.
//

// import UIKit
import Foundation
import Christy_K

// https://stackoverflow.com/questions/30757193/find-out-if-character-in-string-is-emoji
// Arnold
extension String {
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
                 0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                 0x1F680...0x1F6FF, // Transport and Map
                 0x2600...0x26FF,   // Misc symbols
                 0x2700...0x27BF,   // Dingbats
                 0xFE00...0xFE0F,   // Variation Selectors
                 0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
                 0x1F1E6...0x1F1FF, // Flags
                 65024...65039,
                 8400...8447,
                 9100...9300,
                 127000...127600:
                 return true
            default:
                continue
            }
        }
        return false
    }
}
extension Character {
    var containsEmoji : Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
                 0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                 0x1F680...0x1F6FF, // Transport and Map
                 0x2600...0x26FF,   // Misc symbols
                 0x2700...0x27BF,   // Dingbats
                 0xFE00...0xFE0F,   // Variation Selectors
                 0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
                 0x1F1E6...0x1F1FF, // Flags
                 65024...65039,
                 8400...8447,
                 9100...9300,
                 127000...127600:
                return true
            default:
                continue
            }
        }
        return false
    }
}

public final class JSONObject : CustomStringConvertible, Collection {
    public typealias DictionaryType = Dictionary<String, CustomStringConvertible>

    // MARK:- Collection
    public typealias Indices = DictionaryType.Indices
    public typealias Iterator = DictionaryType.Iterator
    public typealias SubSequence = DictionaryType.SubSequence

    public var startIndex: Index { return map.startIndex }
    public var endIndex: DictionaryType.Index { return map.endIndex }

    public subscript(position: Index) -> Iterator.Element { return map[position] }
    public subscript(bounds: Range<Index>) -> SubSequence { return map[bounds] }
    public var indices: Indices { return map.indices }

    public subscript(key: CustomStringConvertible)-> CustomStringConvertible? {
        get { return map[key.description] }
        set { map[key.description] = newValue }
    }
    public subscript(key: String)-> CustomStringConvertible? {
        get { return map[key] }
        set { map[key] = newValue }
    }
    public func index(after i: Index) -> Index {
        return map.index(after: i)
    }
    public var keys : [String] {
        return Array(self.map.keys)
    }

    //Sequence: iteration is implemented here
    public func makeIterator() -> DictionaryIterator<String, CustomStringConvertible> {
        return map.makeIterator()
    }

    //IndexableBase
    public typealias Index = DictionaryType.Index

    /**
        JSONObject.NULL is equivalent to the value that JavaScript calls null,
        while Swift's null is equivalent to the value that Javascript calls
        undefined.
     */
    public final class Null : Hashable, Equatable, CustomStringConvertible {

        /**
            A Null object is equal to the null value and to itself.
         */
        public static func ==(lhs: Null, rhs: Null) -> Bool {
            return true
        }
        public static func ==(lhs: Any?, rhs: Null) -> Bool {
            return lhs == nil || type(of: lhs) == type(of: rhs)
        }
        public static func ==(lhs: Null, rhs: Any?) -> Bool {
            return rhs == nil || type(of: rhs) == type(of: lhs)
        }

        public var description: String {
            return "null"
        }

        public func hash(into hasher: inout Hasher) {
            hasher.combine(7)
        }
    }

    /**
        The map where the JSONObject's properties are kept.
     */
    private var map : DictionaryType = [:]

    /**
        It is sometimes more convenient and less ambiguous to have a
        NULL object thatn to use Swift's nil value.
     */
    public static let NULL = Null()

    private static let endChar = Character("\0")

    // MARK:- Initialization

    public init() {}

    /**
        Construct a JSONObject from a subset of another JSONObject.

        An array of strings is used to identify the keys to be copied. Missing
        keys are ignored.

        - parameter jo: A JSONObject
        - parameter keys: An array of strings
     */
    public convenience init(_ jo: JSONObject, _ keys: [String]) {
        self.init()

        keys.forEach({
            do {
                try putOnce($0, jo.opt($0))
            } catch {
                // Ignore
            }
        })
    }

    /**
        Construct a JSONObject from a JSONTokener.

        - parameter x: A JSONTokener object containing the source string
     */
    public convenience init(_ x: JSONTokener) throws {
        self.init()
        var c : Character
        var key : String

        if (try x.nextClean() != "{") {
            throw JSONError.undefined("A JSONObject text must begin with '{'")
        }

        while true {
            try c = x.nextClean()

            switch c {
            case JSONObject.endChar:
                throw JSONError.undefined("A JSONObject text must end with '}'")
            case "}":
                return
            default:
                try x.back()
                key = try x.nextValue()?.description ?? "missingValue"
            }

            // The key is follwoed by ':'
            c = try x.nextClean()

            if c != ":" {
                throw JSONError.undefined("Expected a ':' after a key")
            }
            try putOnce(key, try x.nextValue())

            // Pairs are separated by ','
            let val = try x.nextClean()
            switch val {
            case ";", ",":
                if try x.nextClean() == "}" {
                    return
                }
                try x.back()
            case "}":
                return
            default:
                throw JSONError.undefined("Expected a ',' or '}'")
            }
        }
    }

    /**
        Construct a JSONObject from a Map

        - parameter map: Key/Value pairs to initialize the JSONObject
     */
    public convenience init(_ map: Dictionary<String,CustomStringConvertible>) {
        self.init()

        map.forEach({ (k,v) in
            self.map[k] = JSONObject.wrap(v.description)
        })
    }

    /**
     */
    // MARK:- TODO Construct using reflection

    /**
        Construct a JSONObject from a source JSON text string.

        This is the most commonly used.

        - parameter src: A source string beginning with '{' and ending with '}'
     */
    public convenience init(_ src: String) throws {
        try self.init(JSONTokener(src))
    }

    /**
        Construct a JSONObject from a source JSON string as a byte array.

        This is the most commonly used.

        - parameter src: A source byte array beginning with '{' and ending with '}'
     */
    public convenience init(_ src: Data) throws {
        guard let utf8 = src.utf8String() else {
            throw JSONError.undefined("JSONObject.init(data) was not a valid utf8 representation")
        }
        let tokener = JSONTokener(utf8)
        try self.init(tokener)
    }

    // MARK:- Helpers

    public func containsKey(_ key: String) -> Bool {
        return self.map[key] != nil 
    }

    // MARK:- Insertion

    /**
        Accumulate values under a key. It is similar to the put method except
        that if there is already an object stored under the key then a JSONArray
        is stored under the key to hold all of the accumulated values.

        If there is already a JSONArray, then the new value is appended to it. In
        contrast, the put method replaces the previous value.

        If only one value is accumulated that is not a JSONArray, then the result
        will be the same as using put. But if multiple values are accumulated,
        then the result will be like append.

        - parameter key: A key string
        - parameter value: An object to be accumulated under the key
     */
    @discardableResult
    public func accumulate<T: CustomStringConvertible>(_ key: String, _ value: T) throws -> JSONObject {
        try JSONObject.testValidity(value)

        if let obj = opt(key) {
            if obj is JSONArray {
                try put(key, (obj as! JSONArray).putAny(value))
            } else {
                try put(key, JSONArray().putAny(obj).putAny(value))
            }
        } else {
            try put(key, value is JSONArray ? value: JSONArray().putAny(value))
        }
        return self
    }
    public func accumulate(_ key: String, _ value: CustomStringConvertible) throws -> JSONObject {
        try JSONObject.testValidity(value)

        if let obj = opt(key) {
            if obj is JSONArray {
                try put(key, (obj as! JSONArray).putAny(value))
            } else {
                try put(key, JSONArray().putAny(obj).putAny(value))
            }
        } else {
            if let jaVal = value as? JSONArray {
                try put(key, jaVal)
            } else {
                try put(key, JSONArray().putAny(value))
            }
        }
        return self
    }

    /**
        Append values to the array under a key. If the key does not exist in the
        JSONObject, then the key is put in the JSONObject with its value being a
        JSONArray contianing the value parameter.

        If the key was already associated with a JSONArray, then the value parameter
        is appended to it.

        - parameter key: The key string
        - parameter value: The value to be accumulated
     */
    @discardableResult
    public func append<T: CustomStringConvertible>(_ key: String, value: T) throws -> JSONObject {
        try JSONObject.testValidity(value)

        if let obj = opt(key) {
            if obj is JSONArray {
                try put(key, (obj as! JSONArray).putAny(value))
            } else {
                throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
            }
        } else {
            try put(key, JSONArray().putAny(value))
        }
        return self
    }
    /**
        Append values to the array under a key. If the key does not exist in the
        JSONObject, then the key is put in the JSONObject with its value being a
        JSONArray contianing the value parameter.

        If the key was already associated with a JSONArray, then the value parameter
        is appended to it.

        - parameter key: The key string
        - parameter value: The value to be accumulated
     */
    @discardableResult
    public func append(_ key: String, value: CustomStringConvertible) throws -> JSONObject {
        try JSONObject.testValidity(value)

        if let obj = opt(key) {
            if obj is JSONArray {
                try put(key, (obj as! JSONArray).putAny(value))
            } else {
                throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
            }
        } else {
            try put(key, JSONArray().putAny(value))
        }
        return self
    }

    /**
        Produce a string from a double. The string "null" will be returned if the
        number is not finite.
     */
    public static func doubleToString(_ double: Double) -> String {
        if double == Double.infinity || double == Double.nan {
            return "null"
        }
        // Shave off trailing zeros and decimal point if possible
        var string = String(double)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }

    /**
        Get the value object associated with a key.
     */
    public func get(_ key: String) throws -> CustomStringConvertible {
        guard let obj = opt(key) else {
            throw JSONError.undefined("JSONObject[\(key)] not found.")
        }
        return obj
    }
    /**
        Get the value object associated with a key.
     */
    public func get<T: JSONCodingKey>(_ key: T) throws -> CustomStringConvertible {
        guard let obj = opt(key.rawValue) else {
            throw JSONError.undefined("JSONObject[\(key)] not found.")
        }
        return obj
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The type of enum expected
     */
    public func getEnum<T: RawRepresentable>(_ type: T.Type, _ key: String) throws -> T {
        guard let obj = optEnum(type, key) else {
            throw JSONError.undefined("JSONObject[\(key)] is not an enum of type \(type).")
        }
        return obj
    }
    /**
        Get the enum value associated with a key.

        - parameter type: The type of enum expected
     */
    public func getEnum<T: RawRepresentable, U: JSONCodingKey>(_ type: T.Type, _ key: U) throws -> T {
        guard let obj = optEnum(type, key.rawValue) else {
            throw JSONError.undefined("JSONObject[\(key)] is not an enum of type \(type).")
        }
        return obj
    }

    /**
        Get the bytes back from a base64 encoded string associated
        with a key.
     */
    public func getBase64Data(_ key: String) throws -> Data {
        guard let data = try get(key).description.base64Decoded else {
            throw JSONError.undefined("JSONObject[\(key)] is not a base64 encoded string.")
        }
        return data
    }

    /**
        Get the bytes back from a base64 encoded string associated
        with a key.
     */
    public func getBase64Data<T: JSONCodingKey>(_ key: T) throws -> Data {
        guard let data = try get(key.rawValue).description.base64Decoded else {
            throw JSONError.undefined("JSONObject[\(key)] is not a base64 encoded string.")
        }
        return data
    }

    /**
        Get the bytes back associated with a key.
     */
    public func getByteArray(_ key: String) throws -> Data {
        guard let data = try get(key).description.base64Decoded else {
            throw JSONError.undefined("JSONObject[\(key)] is not a byte array.")
        }
        return data
    }

    /**
        Get the bytes back associated with a key.
     */
    public func getByteArray<T: JSONCodingKey>(_ key: T) throws -> Data {
        guard let data = try get(key.rawValue).description.base64Decoded else {
            throw JSONError.undefined("JSONObject[\(key)] is not a byte array.")
        }
        return data
    }

    /**
        Get the boolean value associated witn an key.

        The string values "true" and "false" are converted to boolean.
     */
    public func getBool(_ key: String) throws -> Bool {
        guard let bool = Bool(try get(key).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a boolean.")
        }
        return bool
    }

    /**
        Get the boolean value associated witn an key.

        The string values "true" and "false" are converted to boolean.
     */
    public func getBool<T: JSONCodingKey>(_ key: T) throws -> Bool {
        guard let bool = Bool(try get(key.rawValue).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a boolean.")
        }
        return bool
    }

    /**
        Get the Decimal value associated with an key.
     */
    public func getDecimal(_ key: String) throws -> Decimal {
        guard let dec = Decimal(string: try get(key).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a decimal.")
        }
        return dec
    }

    /**
        Get the Decimal value associated with an key.
     */
    public func getDecimal<T: JSONCodingKey>(_ key: T) throws -> Decimal {
        guard let dec = Decimal(string: try get(key.rawValue).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a decimal.")
        }
        return dec
    }

    /**
        Get the double value associated with an key.
     */
    public func getDouble(_ key: String) throws -> Double {
        guard let double = Double(try get(key).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a double.")
        }
        return double
    }

    /**
        Get the double value associated with an key.
     */
    public func getDouble<T: JSONCodingKey>(_ key: T) throws -> Double {
        guard let double = Double(try get(key.rawValue).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a double.")
        }
        return double
    }

    /**
        Get the Int value associated with an key.
     */
    public func getInt(_ key: String) throws -> Int {
        guard let int = Int(try get(key).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a number.")
        }
        return int
    }

    /**
        Get the Int value associated with an key.
     */
    public func getInt<T: JSONCodingKey>(_ key: T) throws -> Int {
        guard let int = Int(try get(key.rawValue).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a number.")
        }
        return int
    }

    /**
        Get the Int value associated with an key.
     */
    public func getInt64(_ key: String) throws -> Int64 {
        guard let int = Int64(try get(key).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a number.")
        }
        return int
    }

    /**
        Get the Int value associated with an key.
     */
    public func getInt64<T: JSONCodingKey>(_ key: T) throws -> Int64 {
        guard let int = Int64(try get(key.rawValue).description) else {
            throw JSONError.undefined("JSONObject[\(key)] is not a number.")
        }
        return int
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func getJSONArray(_ key: String) throws -> JSONArray {
        guard let obj = try get(key) as? JSONArray else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
        }
        return obj
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func getJSONArray<T: JSONCodingKey>(_ key: T) throws -> JSONArray {
        guard let obj = try get(key.rawValue) as? JSONArray else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
        }
        return obj
    }

    /**
        Get the JSONObject associated with an key.
     */
    public func getJSONObject(_ key: String) throws -> JSONObject {
        guard let obj = try get(key) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return obj
    }

    /**
        Get the JSONObject associated with an key.
     */
    public func getJSONObject<T: JSONCodingKey>(_ key: T) throws -> JSONObject {
        guard let obj = try get(key.rawValue) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return obj
    }

    /**
        Get the JSONObject associated with an key.
     */
    public func getObject<T: JSONObjectable>(_ key: String) throws -> T {
        guard let obj = try get(key) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return try T(obj)
    }

    /**
        Get the JSONObject associated with an key.
     */
    public func getObject<T: JSONCodingKey, U: JSONObjectable>(_ key: T) throws -> U {
        guard let obj = try get(key.rawValue) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return try U(obj)
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func getArray<T: JSONArrayable>(_ key: String) throws -> T {
        guard let obj = try get(key) as? JSONArray else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
        }
        return try T(obj)
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func getArray<T: JSONCodingKey, U: JSONArrayable>(_ key: T) throws -> U {
        guard let obj = try get(key.rawValue) as? JSONArray else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
        }
        return try U(obj)
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func getArray<T: JSONObjectable>(_ key: String) throws -> [T] {
        guard let arr = try get(key) as? JSONArray else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
        }
        return try arr.compactMap({ try T(JSONObject($0.description)) })
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func getArray<T: JSONCodingKey, U: JSONObjectable>(_ key: T) throws -> [U] {
        guard let arr = try get(key.rawValue) as? JSONArray else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONArray.")
        }
        return try arr.compactMap({ try U(JSONObject($0.description)) })
    }

    /**
        Get the HashMap associated with an key.

        NOTE: WILL NOT WORK FOR NUMERIC TYPES
     */
    public func getHashMap<U: Hashable, V>(_ key: String, _ fn: ((String, CustomStringConvertible) -> (U, V)? )) throws -> Dictionary<U,V> {
        guard let obj = try get(key) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return obj.map.compact(fn)
    }

    /**
        Get the HashMap associated with an key.

        NOTE: WILL NOT WORK FOR NUMERIC TYPES
     */
    public func getHashMap<U: JSONKeyable, V>(_ key: String, _ fn: (CustomStringConvertible) -> V?) throws -> Dictionary<U,V> {
        guard let obj = try get(key) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return obj.map.compact({
            guard let k = U($0) else { return nil }
            guard let v = fn($1) else { return nil }
            return (k,v)
        })
    }

    /**
        Get the HashMap associated with an key.

        NOTE: WILL NOT WORK FOR NUMERIC TYPES
     */
    public func getHashMap<T:JSONCodingKey, U: JSONKeyable, V>(_ key: T, _ fn: (CustomStringConvertible) -> V?) throws -> Dictionary<U,V> {
        guard let obj = try get(key.rawValue) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return obj.map.compact({
            guard let k = U($0) else { return nil }
            guard let v = fn($1) else { return nil }
            return (k,v)
        })
    }

    public func optHashMap<T: JSONCodingKey, U: JSONKeyable, V>(_ key: T, fn: (CustomStringConvertible) -> V?) -> Dictionary<U,V>? {
        guard let obj = opt(key.rawValue) as? JSONObject else {
            return nil
        }
        return obj.map.compact({
            guard let k = U($0) else { return nil }
            guard let v = fn($1) else { return nil }
            return (k,v)
        })
    }

    /**
        Get the HashMap associated with an key.
     */
    public func getHashMap<T: JSONCodingKey, U: Hashable, V>(_ key: T, _ fn: ((String, CustomStringConvertible) -> (U, V)? )) throws -> Dictionary<U,V>  {
        guard let obj = try get(key.rawValue) as? JSONObject else {
            throw JSONError.undefined("JSONObject[\(key)] is not a JSONObject.")
        }
        return obj.map.compact(fn)
        //return obj.map.compactMapValues({ $0.description as? U })
    }

    public func optHashMap<T: JSONCodingKey, U: Hashable, V>(_ key: T, _ fn: ((String, CustomStringConvertible) -> (U, V)? )) -> Dictionary<U,V>?  {
        guard let obj =  opt(key.rawValue) as? JSONObject else {
            return nil
        }
        return obj.map.compact(fn)
        //return obj.map.compactMapValues({ $0.description as? U })
    }

    /**
        Get the String associated with an key.
     */
    public func getString(_ key: String) throws -> String {
        return try get(key).description
    }


    /**
        Get the String associated with an key.
     */
    public func getString<T: JSONCodingKey>(_ key: T) throws -> String {
        return try get(key.rawValue).description
    }

    /**
        Get an array of field names from a JSONObject.

        - parameter jo: The JSONObject to extract keys
     */
    public static func getNames(_ jo: JSONObject) -> [String] {
        let count = jo.count
        if count == 0 {
            return []
        }
        return Array(jo.map.keys)
    }

    // MARK:- Interaction

    /**
        Determine if the JSONObject contains a specific key.
     */
    public func has(_ key: String) -> Bool {
        return map[key] != nil
    }

    /**
        Determine if the JSONObject contains a specific key.
     */
    public func has<T: JSONCodingKey>(_ key: T) -> Bool {
        return map[key.rawValue] != nil
    }

    /**
        Increment a property of a JSONObject. If there is no such
        property, create one with a value of 1.

        If there is such a property, and if it is an Int, Double,
        or Float, then add one to it.
     */
    @discardableResult
    public func increment(_ key: String) throws -> JSONObject {
        if let obj = opt(key) {
            if obj is Decimal {
                try put(key, (obj as! Decimal) + 1)
            } else if obj is Int {
                try put(key, (obj as! Int) + 1)
            } else if obj is Double {
                try put(key, (obj as! Double) + 1)
            } else if obj is Float {
                try put(key, (obj as! Float) + 1)
            } else {
                throw JSONError.undefined("Unable to increment [\(key)].")
            }
        } else {
            try put(key, 1)
        }
        return self
    }

    /**
        Increment a property of a JSONObject. If there is no such
        property, create one with a value of 1.

        If there is such a property, and if it is an Int, Double,
        or Float, then add one to it.
     */
    @discardableResult
    public func increment<T: JSONCodingKey>(_ key: T) throws -> JSONObject {
        if let obj = opt(key.rawValue) {
            if obj is Decimal {
                try put(key, (obj as! Decimal) + 1)
            } else if obj is Int {
                try put(key, (obj as! Int) + 1)
            } else if obj is Double {
                try put(key, (obj as! Double) + 1)
            } else if obj is Float {
                try put(key, (obj as! Float) + 1)
            } else {
                throw JSONError.undefined("Unable to increment [\(key)].")
            }
        } else {
            try put(key, 1)
        }
        return self
    }

    /**
        Determine if the value associated with the key is null or if there
        is no value.
     */
    public func isNull(_ key: String) -> Bool {
        return opt(key) == JSONObject.NULL
    }

    /**
        Determine if the value associated with the key is null or if there
        is no value.
     */
    public func isNull<T: JSONCodingKey>(_ key: T) -> Bool {
        return opt(key.rawValue) == JSONObject.NULL
    }

    /**
        Get the number of keys stored in the JSONObject.
     */
    public var count : Int {
        return map.count
    }

    /**
        Produce a JSONArray containing the names of the elements
        of this JSONObject.
     */
    public func names() -> JSONArray {
        let ja = JSONArray()
        let keys = Array(self.map.keys)
        var i = 0

        while i < keys.count {
            ja.put(keys[i])
            i += 1
        }
        return ja
    }

    /**
        Get an optional value associated with a key.
     */
    public func opt(_ key: String) -> CustomStringConvertible? {
        return self.map[key]
    }

    /**
        Get an optional value associated with a key.
     */
    public func opt<T: JSONCodingKey>(_ key: T) -> CustomStringConvertible? {
        return self.map[key.rawValue]
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The expected type to be returned
        - parameter key: The key associated with the underlying value
     */
    public func optEnum<T: RawRepresentable>(_ type: T.Type, _ key: String) -> T? {
        return optEnum(type, key, nil)
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The expected type to be returned
        - parameter key: The key associated with the underlying value
     */
    public func optEnum<T: RawRepresentable, U: JSONCodingKey>(_ type: T.Type, _ key: U) -> T? {
        return optEnum(type, key.rawValue, nil)
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The expected type to be returned
        - parameter key: The key associated with the underlying value
        - parameter default: The default value to be returned
     */
    public func optEnum<T: RawRepresentable>(_ type: T.Type, _ key: String, _ default: T?) -> T? {
        guard let obj = opt(key) as? T.RawValue, let t = T(rawValue: obj) else {
            return `default`
        }
        return t
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The expected type to be returned
        - parameter key: The key associated with the underlying value
        - parameter default: The default value to be returned
     */
    public func optEnum<T: RawRepresentable, U: JSONCodingKey>(_ type: T.Type, _ key: U, _ default: T?) -> T? {
        guard let obj = opt(key.rawValue) as? T.RawValue, let t = T(rawValue: obj) else {
            return `default`
        }
        return t
    }

    public func optBase64Data(_ key: String) -> Data? {
        return (opt(key) as? String)?.base64Decoded
    }

    public func optBase64Data<T: JSONCodingKey>(_ key: T) -> Data? {
        return (opt(key.rawValue) as? String)?.base64Decoded
    }

    public func optByteArray(_ key: String) -> Data? {
        return (opt(key) as? String)?.base64Decoded
    }

    public func optByteArray<T: JSONCodingKey>(_ key: T) -> Data? {
        return (opt(key.rawValue) as? String)?.base64Decoded
    }

    /**
        Get the optional boolean value associated with an key.

        It returns false if there is no value at that key, or if
        the value is not boolean.

        - parameter key: The key associated with the underlying value
     */
    public func optBool(_ key: String) -> Bool? {
        return optBool(key, false)
    }


    /**
        Get the optional boolean value associated with an key.

        It returns false if there is no value at that key, or if
        the value is not boolean.

        - parameter key: The key associated with the underlying value
     */
    public func optBool<T: JSONCodingKey>(_ key: T) -> Bool? {
        return optBool(key.rawValue, nil)
    }

    /**
        Get the optional boolean value associated with an key.

        It returns the default if there is no key at that index
        or if it is not a boolean.

        - parameter key: The key associated with the underlying value
        - parameter default: A boolean default
     */
    public func optBool(_ key: String, _ default: Bool?) -> Bool? {
        do {
            return try getBool(key)
        } catch { return `default` }
    }

    /**
        Get the optional boolean value associated with an key.

        It returns the default if there is no key at that index
        or if it is not a boolean.

        - parameter key: The key associated with the underlying value
        - parameter default: A boolean default
     */
    public func optBool<T: JSONCodingKey>(_ key: T, _ default: Bool?) -> Bool? {
        do {
            return try getBool(key.rawValue)
        } catch { return `default` }
    }

    /**
        Get the optional Double value associated with an key. NaN is
        returned if there is no value for the key, or if the value is
        not a number and cannot be converted into one.

        - parameter key: The key associated with the underlying value
     */
    public func optDouble(_ key: String) -> Double? {
        return optDouble(key, nil)
    }

    /**
        Get the optional Double value associated with an key. NaN is
        returned if there is no value for the key, or if the value is
        not a number and cannot be converted into one.

        - parameter key: The key associated with the underlying value
     */
    public func optDouble<T: JSONCodingKey>(_ key: T) -> Double? {
        return optDouble(key.rawValue, nil)
    }

    /**
        Get the Double value associated with an key.

        The default is returned if there is no proper value for
        the key.

        - parameter key: The key associated with the underlying value
        - parameter default: A Double default
     */
    public func optDouble(_ key: String, _ default: Double?) -> Double? {
        do {
            return try getDouble(key)
        } catch { return `default` }
    }

    /**
        Get the Double value associated with an key.

        The default is returned if there is no proper value for
        the key.

        - parameter key: The key associated with the underlying value
        - parameter default: A Double default
     */
    public func optDouble<T: JSONCodingKey>(_ key: T, _ default: Double?) -> Double? {
        do {
            return try getDouble(key.rawValue)
        } catch { return `default` }
    }

    /**
        Get the optional Decimal value associated with an key.

        The default is returned if there is no proper Decimal value found.

        - parameter key: The key associated with the underlying value
     */
    public func optDecimal(_ key: String) -> Decimal? {
        return optDecimal(key, nil)
    }

    /**
        Get the optional Decimal value associated with an key.

        The default is returned if there is no proper Decimal value found.

        - parameter key: The key associated with the underlying value
     */
    public func optDecimal<T: JSONCodingKey>(_ key: T) -> Decimal? {
        return optDecimal(key.rawValue, nil)
    }

    /**
        Get the optional Decimal value associated with an key.

        The default is returned if there is no proper Decimal value found.

        - parameter key: The key associated with the underlying value
        - parameter default: The default value to return
     */
    public func optDecimal(_ key: String, _ default: Decimal?) -> Decimal? {
        do {
            return try getDecimal(key)
        } catch { return `default` }
    }

    /**
        Get the optional Decimal value associated with an key.

        The default is returned if there is no proper Decimal value found.

        - parameter key: The key associated with the underlying value
        - parameter default: The default value to return
     */
    public func optDecimal<T: JSONCodingKey>(_ key: T, _ default: Decimal?) -> Decimal? {
        do {
            return try getDecimal(key.rawValue)
        } catch { return `default` }
    }

    /**
        Get the optional Int value associated with an key.

        Zero is returned if there is no proper Int value.

        - parameter key: The key associated with the underlying value
        - parameter default: The default Int to return
     */
    public func optInt64(_ key: String) -> Int64? {
        return optInt64(key, nil)
    }

    /**
        Get the optional Int value associated with an key.

        Zero is returned if there is no proper Int value.

        - parameter key: The key associated with the underlying value
        - parameter default: The default Int to return
     */
    public func optInt64<T: JSONCodingKey>(_ key: T) -> Int64? {
        return optInt64(key.rawValue, nil)
    }


    /**
        Get the Int value associated with an key.

        The default is returned if there is no proper value for
        the index.

        - parameter key: The key associated with the underlying value
        - parameter default: An Int default
     */
    public func optInt64(_ key: String, _ default: Int64?) -> Int64? {
        do {
            return try getInt64(key)
        } catch { return `default` }
    }

    /**
        Get the Int value associated with an key.

        The default is returned if there is no proper value for
        the index.

        - parameter key: The key associated with the underlying value
        - parameter default: An Int default
     */
    public func optInt64<T: JSONCodingKey>(_ key: T, _ default: Int64?) -> Int64? {
        do {
            return try getInt64(key.rawValue)
        } catch { return `default` }
    }


    /**
        Get the optional Int value associated with an key.

        Zero is returned if there is no proper Int value.

        - parameter key: The key associated with the underlying value
        - parameter default: The default Int to return
     */
    public func optInt(_ key: String) -> Int? {
        return optInt(key, nil)
    }

    /**
        Get the optional Int value associated with an key.

        Zero is returned if there is no proper Int value.

        - parameter key: The key associated with the underlying value
        - parameter default: The default Int to return
     */
    public func optInt<T: JSONCodingKey>(_ key: T) -> Int? {
        return optInt(key.rawValue, nil)
    }

    /**
        Get the Int value associated with an key.

        The default is returned if there is no proper value for
        the index.

        - parameter key: The key associated with the underlying value
        - parameter default: An Int default
     */
    public func optInt(_ key: String, _ default: Int?) -> Int? {
        do {
            return try getInt(key)
        } catch { return `default` }
    }

    /**
        Get the Int value associated with an key.

        The default is returned if there is no proper value for
        the index.

        - parameter key: The key associated with the underlying value
        - parameter default: An Int default
     */
    public func optInt<T: JSONCodingKey>(_ key: T, _ default: Int?) -> Int? {
        do {
            return try getInt(key.rawValue)
        } catch { return `default` }
    }

    /**
        Get the optional JSONArray value associated with an key.

        - parameter key: The key associated with the underlying value
     */
    public func optJSONArray(_ key: String) -> JSONArray? {
        return opt(key) as? JSONArray
    }

    /**
        Get the optional JSONArray value associated with an key.

        - parameter key: The key associated with the underlying value
     */
    public func optJSONArray<T: JSONCodingKey>(_ key: T) -> JSONArray? {
        return opt(key.rawValue) as? JSONArray
    }

    /**
        Get the optional JSONObject value associated with an key.

        - parameter key: The key associated with the underlying value
     */
    public func optJSONObject(_ key: String) -> JSONObject? {
        return opt(key) as? JSONObject
    }

    /**
        Get the optional JSONObject value associated with an key.

        - parameter key: The key associated with the underlying value
     */
    public func optJSONObject<T: JSONCodingKey>(_ key: T) -> JSONObject? {
        return opt(key.rawValue) as? JSONObject
    }

    /**
        Get the JSONObject associated with an key.
    */
    public func optObject<T: JSONObjectable>(_ key: String) -> T? {
        do {
            guard let obj = try get(key) as? JSONObject else {
                return nil
            }
            return try T(obj)
        } catch {
            return nil
        }
    }

    /**
        Get the JSONObject associated with an key.
    */
    public func optObject<T: JSONCodingKey, U: JSONObjectable>(_ key: T) -> U? {
        return optObject(key.rawValue)
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func optArray<T: JSONArrayable>(_ key: String) -> T? {
       do {
           guard let obj = try get(key) as? JSONArray else {
               return nil
           }
            return try T(obj)
       } catch {
           return nil
       }
    }

    /**
        Get the JSONArray associated with an key.
    */
    public func optArray<T: JSONCodingKey, U: JSONArrayable>(_ key: T) -> U? {
        return optArray(key.rawValue)
    }


    /**
        Get the JSONArray associated with an key.
     */
    public func optArray<T: JSONObjectable>(_ key: String) -> [T]? {
        do {
            return try (try get(key) as? JSONArray)?.compactMap({ try T(JSONObject($0.description)) })
        } catch { return nil }
    }

    /**
        Get the JSONArray associated with an key.
     */
    public func optArray<T: JSONCodingKey, U: JSONObjectable>(_ key: T) -> [U]? {
        do {
            return try (try get(key.rawValue) as? JSONArray)?.compactMap({ try U(JSONObject($0.description)) })
        } catch { return nil }
    }

    /**
        Get the HashMap associated with an key.
     */
    public func optHashMap<U>(_ key: String) -> [String:U]? {
        do {
            guard let obj = try get(key) as? [String:U] else {
                return nil
            }
            return obj
        } catch {
            return nil
        }
    }


    /**
        Get the HashMap associated with an key.
     */
    public func optHashMap<T: JSONCodingKey, U>(_ key: T) -> [String:U]? {
        return optHashMap(key.rawValue)
    }



    /**
        Get the optional String value associated with an key.

        The default is returned if there is no proper String value found.

        - parameter key: The key associated with the underlying value
     */
    public func optString(_ key: String) -> String? {
        return optString(key, "")
    }

    /**
        Get the optional String value associated with an key.

        The default is returned if there is no proper String value found.

        - parameter key: The key associated with the underlying value
     */
    public func optString<T: JSONCodingKey>(_ key: T) -> String? {
        return optString(key.rawValue, nil)
    }


    /**
        Get the optional JSONObject value associated with an key.

        The default is returned if there is no proper JSONObject value found.

        - parameter key: The key associated with the underlying value
        - parameter default: The default value to return
     */
    public func optString(_ key: String, _ default: String?) -> String? {
        return (opt(key) as? String) ?? `default`
    }

    /**
        Get the optional JSONObject value associated with an key.

        The default is returned if there is no proper JSONObject value found.

        - parameter key: The key associated with the underlying value
        - parameter default: The default value to return
     */
    public func optString<T: JSONCodingKey>(_ key: T, _ default: String?) -> String? {
        return (opt(key.rawValue) as? String) ?? `default`
    }


    // MARK:- Interaction

    // TODO FIXME: Java uses reflection
    //private func populateMap(_ )

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func putObjectArray<T: JSONCodingKey, U: JSONObjectable>(_ key: T, _ value: [U]?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = try JSONArray().put(value)
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func putObjectArray<T: JSONObjectable>(_ key: String, _ value: [T]?) throws -> JSONObject {
        if let value = value {
            map[key] = try JSONArray().put(value)
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: CustomStringConvertible>(_ key: String, _ value: [String:T]?) throws -> JSONObject {
        if let value = value {
            map[key] = JSONObject(value)
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey, U: CustomStringConvertible>(_ key: T, _ value: [String:U]?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = JSONObject(value)
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }


    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONObjectable>(_ key: String, _ value: T?) throws -> JSONObject {
        if let value = value {
            map[key] = try value.encoded()
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey, U: JSONObjectable>(_ key: T, _ value: U?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = try value.encoded()
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONArrayable>(_ key: String, _ value: T?) throws -> JSONObject {
        if let value = value {
            map[key] = try value.encoded()
        } else {
            map.remove(key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey, U: JSONArrayable>(_ key: T, _ value: U?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = try value.encoded()
        } else {
            map.remove(key.rawValue)
        }
        return self
    }

    /**
        Put a JSONObject in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: JSONObject?) throws -> JSONObject {
        if let value = value {
            map[key] = value
        } else {
            map.remove(key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: JSONArray?) throws -> JSONObject {
        if let value = value {
            map[key] = value
        } else {
            map.remove(key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: JSONArray?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = value
        } else {
            map.remove(key.rawValue)
        }
        return self
    }


    /**
        Puts a key/JSONArray pair in the JSONObject, where each element
        in the array is an object that will be converted into a JSONObject
        via it's JSONObjectable conformance.

        - parameter key: The key associated with the underlying value
        - parameter values: An array of JSONObjectable objects
     */
    @discardableResult
    public func putJSONArrayOfObjects<T: JSONCodingKey>(_ key: T, _ values: [JSONObjectable]?) throws -> JSONObject {
        if let values = values {
            let jsonArray = JSONArray()
            values.forEach({
                do {
                    jsonArray.put(try $0.encoded())
                } catch {}
            })
            map[key.rawValue] = jsonArray
        } else {
            map.remove(key.rawValue)
        }
        return self
    }

    /**
        Puts a key/JSONArray pair in the JSONObject, where each element
        in the array is an object that will be converted into a JSONObject
        via it's JSONObjectable conformance.

        - parameter key: The key associated with the underlying value
        - parameter values: An array of JSONObjectable objects
     */
    @discardableResult
    public func putJSONArrayOfObjects(_ key: String, _ values: [JSONObjectable]?) throws -> JSONObject {
        if let values = values {
            let jsonArray = JSONArray()
            values.forEach({
                do {
                    jsonArray.put(try $0.encoded())
                } catch {}
            })
            map[key] = jsonArray
        } else {
            map.remove(key)
        }
        return self 
    }

    /**
        Put a key/bool pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A boolean value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: Bool?) throws -> JSONObject {
        if let value = value {
            map[key] = value
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }

    /**
        Put a key/bool pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A boolean value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: Bool?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = value
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: Data?) throws -> JSONObject {
        if let value = value {
            map[key] = value
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }

    /**
        Put a byte array in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A byte array value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: Data?) throws -> JSONObject {
        if let value = value {
            map[key.rawValue] = value
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }

    /**
        Put a key/JSONArray pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A collection to be converted into JSONArray
     */
    @discardableResult
    public func put<T: CustomStringConvertible>(_ key: String, _ value: [T]) throws -> JSONObject {
        map[key] = JSONArray(value)
        return self
    }

    /**
        Put a key/JSONArray pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A collection to be converted into JSONArray
     */
    @discardableResult
    public func put<T: CustomStringConvertible, U: JSONCodingKey>(_ key: U, _ value: [T]) throws -> JSONObject {
        map[key.rawValue] = JSONArray(value)
        return self
    }

    /**
        Put a key/Double pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A Double value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: Double) throws -> JSONObject {
        map[key] = value
        return self
    }

    /**
        Put a key/Double pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A Double value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: Double) throws -> JSONObject {
        map[key.rawValue] = value
        return self
    }

    /**
        Put a key/Int pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: An Int value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: Int) throws -> JSONObject {
        map[key] = value
        return self
    }

    /**
        Put a key/Int pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: An Int value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: Int) throws -> JSONObject {
        map[key.rawValue] = value
        return self
    }

    /**
        Put a key/Float pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A Float value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: Float) throws -> JSONObject {
        map[key] = value
        return self
    }

    /**
        Put a key/Float pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A Float value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: Float) throws -> JSONObject {
        map[key.rawValue] = value
        return self
    }

    /**
        Put a key/Map pair in the JSONObject. The map will be converted to
        a JSONObject

        - parameter key: The key associated with the underlying value
        - parameter value: A map value to associate with the key
     */
    @discardableResult
    public func put(_ key: String, _ value: Dictionary<String,CustomStringConvertible>) throws -> JSONObject {
        map[key] = JSONObject(value)
        return self
    }

    /**
        Put a key/Map pair in the JSONObject. The map will be converted to
        a JSONObject

        - parameter key: The key associated with the underlying value
        - parameter value: A map value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T, _ value: Dictionary<String,CustomStringConvertible>) throws -> JSONObject {
        map[key.rawValue] = JSONObject(value)
        return self
    }

    /**
        Put a key/JSONable pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A JSONable value to associate with the key
     */
    @discardableResult
    public func put<T: CustomStringConvertible>(_ key: String?, _ value: T?) throws -> JSONObject {
        guard let key = key else {
            throw JSONError.undefined("Nil key")
        }

        if let value = value {
            try JSONObject.testValidity(value)
            map[key] = value
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }
    /**
        Put a key/JSONable pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A JSONable value to associate with the key
     */
    @discardableResult
    public func put<T: CustomStringConvertible, U: JSONCodingKey>(_ key: U?, _ value: T?) throws -> JSONObject {
        guard let key = key else {
            throw JSONError.undefined("Nil key")
        }

        if let value = value {
            try JSONObject.testValidity(value)
            map[key.rawValue] = value
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }
    /**
        Put a key/JSONable pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A JSONable value to associate with the key
     */
    @discardableResult
    public func put(_ key: String?, _ value: CustomStringConvertible?) throws -> JSONObject {
        guard let key = key else {
            throw JSONError.undefined("Nil key")
        }

        if let value = value {
            try JSONObject.testValidity(value)
            map[key] = value
        } else {
            map.removeValue(forKey: key)
        }
        return self
    }
    /**
        Put a key/JSONable pair in the JSONObject.

        - parameter key: The key associated with the underlying value
        - parameter value: A JSONable value to associate with the key
     */
    @discardableResult
    public func put<T: JSONCodingKey>(_ key: T?, _ value: CustomStringConvertible?) throws -> JSONObject {
        guard let key = key else {
            throw JSONError.undefined("Nil key")
        }

        if let value = value {
            try JSONObject.testValidity(value)
            map[key.rawValue] = value
        } else {
            map.removeValue(forKey: key.rawValue)
        }
        return self
    }

    /**
        Put a key/value pair in the JSONObject, but only if there is not
        already a member with that name.
     */
    @discardableResult
    public func putOnce<T: CustomStringConvertible>(_ key: String?, _ value: T?) throws -> JSONObject {
        if let key = key, let value = value {
            if opt(key) != nil {
                throw JSONError.undefined("Duplicate key \\\(key)\\")
            }
            try put(key, value)
        }
        return self
    }
    /**
        Put a key/value pair in the JSONObject, but only if there is not
        already a member with that name.
     */
    @discardableResult
    public func putOnce<T: CustomStringConvertible, U: JSONCodingKey>(_ key: U?, _ value: T?) throws -> JSONObject {
        if let key = key, let value = value {
            if opt(key.rawValue) != nil {
                throw JSONError.undefined("Duplicate key \\\(key)\\")
            }
            try put(key.rawValue, value)
        }
        return self
    }
    /**
        Put a key/value pair in the JSONObject, but only if there is not
        already a member with that name.
     */
    @discardableResult
    public func putOnce(_ key: String?, _ value: CustomStringConvertible?) throws -> JSONObject {
        if let key = key, let value = value {
            if opt(key) != nil {
                throw JSONError.undefined("Duplicate key \\\(key)\\")
            }
            try put(key, value)
        }
        return self
    }
    /**
        Put a key/value pair in the JSONObject, but only if there is not
        already a member with that name.
     */
    @discardableResult
    public func putOnce<T: JSONCodingKey>(_ key: T?, _ value: CustomStringConvertible?) throws -> JSONObject {
        if let key = key, let value = value {
            if opt(key.rawValue) != nil {
                throw JSONError.undefined("Duplicate key \\\(key)\\")
            }
            try put(key.rawValue, value)
        }
        return self
    }

    /**
        Put a key/value pair in the JSONObject, but only if they are non-nil.
     */
    @discardableResult
    public func putOpt<T: CustomStringConvertible>(_ key: String?, _ value: T?) throws -> JSONObject {
        if let key = key, let value = value {
            try put(key, value)
        }
        return self
    }
    /**
        Put a key/value pair in the JSONObject, but only if they are non-nil.
     */
    @discardableResult
    public func putOpt<T: CustomStringConvertible, U: JSONCodingKey>(_ key: U?, _ value: T?) throws -> JSONObject {
        if let key = key, let value = value {
            try put(key.rawValue, value)
        }
        return self
    }
    /**
        Put a key/value pair in the JSONObject, but only if they are non-nil.
     */
    @discardableResult
    public func putOpt(_ key: String?, _ value: CustomStringConvertible?) throws -> JSONObject {
        if let key = key, let value = value {
            try put(key, value)
        }
        return self
    }
    /**
        Put a key/value pair in the JSONObject, but only if they are non-nil.
     */
    @discardableResult
    public func putOpt<T: JSONCodingKey>(_ key: T?, _ value: CustomStringConvertible?) throws -> JSONObject {
        if let key = key, let value = value {
            try put(key.rawValue, value)
        }
        return self
    }

    /**
        Produce a string in double quotes with backslash sequences in all
        the right places. A backslash will be inserted within allowing JSON
        text to be delivered in HTML. In JSON text, a string cannot contain a
        control character or an unescaped quote or backslash.
     */
    @discardableResult
    public static func quote(_ src: String) -> String {
        var sb = Substring()
        FastJSONEncoder.quote(src, &sb)
        //try quote(src, &sb)
        return String(sb)
    }

    @discardableResult
    public static func quote(_ src: String, _ sb: inout Substring) -> String {
        FastJSONEncoder.quote(src, &sb)
        //try quote(src, &sb)
        return String(sb)
    }

    /**
        Remove a key and its value, if present.

        - parameter key: The key associated with the object being removed
     */
    public func remove(_ key: String) -> CustomStringConvertible? {
        return map.removeValue(forKey: key)
    }

    /**
        Determine if two JSONObjects are similar.

        They must contain the smae number of keys which must
        be associated with similar values.

        - parameter jo: The JSONObject to compare to
     */
    public func similar(_ jo: JSONObject) -> Bool {
        do {
            let set : Set<String> = Set<String>(self.map.keys)

            if set != Set<String>(self.map.keys) {
                return false
            }

            for key in set {
                let this = try get(key)
                let that = try jo.get(key)

                if let joThis = this as? JSONObject, let joThat = that as? JSONObject {
                    if !joThis.similar(joThat) {
                        return false
                    }
                } else if let jaThis = this as? JSONArray, let jaThat = that as? JSONArray {
                    if !jaThis.similar(jaThat) {
                        return false
                    }
                } else if type(of: this) != type(of: that) {
                    return false
                } else if this.description != that.description {
                    return false
                }
            }
            return true
        } catch {
            return false
        }
    }

    /**
        Try to convert a string into a number, boolean, or nil.

        If the string still can't be converted, return the string.
     */
    public static func stringToValue(_ src: String) -> CustomStringConvertible {
        if src == "" {
            return src
        }
        if src == "true" || src == "TRUE" {
            return true
        }
        if src == "false" || src == "FALSE" {
            return false
        }
        if src == "null" {
            return JSONObject.NULL
        }

        if src.contains(".") {
            if let d = Double(src) {
                return d
            }
        } else {
            if let i = Int(src) {
                return i
            }
        }

        return src
    }

    /**
        Throw an exception if the object is a NaN or infinite number.

        - parameter obj: The object to test
     */
    public static func testValidity(_ obj: CustomStringConvertible) throws {
        if obj is Double {
            if (obj as! Double) == Double.nan || (obj as! Double) == Double.infinity {
                throw JSONError.undefined("JSON does not allow non-finite numbers")
            }
        } else if obj is Float {
            if (obj as! Float) == Float.nan || (obj as! Float) == Float.infinity {
                throw JSONError.undefined("JSON does not allow non-finite numbers")
            }
        } else if obj is Decimal {
            if (obj as! Decimal) == Decimal.nan {
                throw JSONError.undefined("JSON does not allow non-finite numbers")
            }
        }
    }

    /**
        Produce a JSONArray containing the values of the members of
        this JSONObject.

        - parameter keys: A JSONArray containing a list of key strings.
     */
    public func toJSONArray(_ keys: JSONArray) throws -> JSONArray {
        guard keys.count > 0 else { return JSONArray() }

        var i = 0
        let ja = JSONArray()

        while i < keys.count {
            let key = try keys.getString(i)
            let val = opt(key)
            try ja.putAny(val)
            i += 1
        }

        return ja
    }


    // MARK:- String Convertible

    /**
        Make a JSON text of this JSONObject. For compactness, no whitespace is
        added. If this would not result in a syntactically correct JSON text,
        then null will be returned instead.

        WARNING: This method assumes that the data structure is cyclical.
     */
    public func toString() -> String? {
        do {
            return try toString(0)
        } catch {
            return nil
        }
    }

    /**
        Make a pretty printed JSON text of this JSONObject.

        WARNING: This method assumes that the data structure is acyclical.
     */
    public func toString(_ indentFactor: Int) throws -> String {
        var sb = Substring()
        try append(&sb, indentFactor, 0)
        return String(sb)
    }

    /**
        Make a JSON text of a value.

        If the object has a value.toJSONString() method, then that method will
        be used to produce the JSON text. The method is required to produce a strictly
        conforming text.

        If the object does not contain a toJSONString method (which is the most
        common case), then a text will be produced by other means. If the value
        is an array or Collection, then a JSONArray will be made from it and its
        toJSONString method will be called. If the value is a MAP, then a
        JSONObject will be made from it and its toJSONString method will be
        called. Otherwise, the value's toString method will be called, and the
        result will be quoted.
     */
    public static func valueToString(_ obj: CustomStringConvertible) throws -> String? {
        if obj is Data {
            //return base64ToQuotedString(obj as! Data)
            return "\"\((obj as! Data).base64EncodedString())\""
        }
        if obj is Bool {
            return String(obj as! Bool)
        }
        if obj is JSONObjectable {
            return try (obj as! JSONObjectable).encoded().toString()
        }
        if obj is JSONArrayable {
            return try (obj as! JSONArrayable).encoded().toString()
        }
        if obj is JSONObject {
            return (obj as! JSONObject).toString()
        }
        if obj is JSONArray {
            return (obj as! JSONArray).toString()
        }
        if let obj = obj as? Dictionary<String, CustomStringConvertible> {
            return JSONObject(obj).toString()
        }
        if let obj = obj as? [CustomStringConvertible] {
            return JSONArray(obj).toString()
        }
        if obj is Int64 {
            return try numberToString(obj as! Int64)
        }
        if obj is Double {
            return try numberToString(obj as! Double)
        }
        if obj is Float {
            return try numberToString(obj as! Float)
        }
        if obj is Decimal {
            return try numberToString(obj as! Decimal)
        }
        if obj is Int {
            return try numberToString(obj as! Int)
        }
        if obj is UInt8 {
            return try numberToString(obj as! UInt8)
        }
        if obj is UInt16 {
            return try numberToString(obj as! UInt16)
        }
        if obj is UInt32 {
            return try numberToString(obj as! UInt32)
        }
        if obj is UInt64 {
            return try numberToString(obj as! UInt64)
        }
        if obj is Int8 {
            return try numberToString(obj as! Int8)
        }
        if obj is Int16 {
            return try numberToString(obj as! Int16)
        }
        if obj is Int32 {
            return try numberToString(obj as! Int32)
        }
        if obj is Int64 {
            return try numberToString(obj as! Int64)
        }
        /*if obj is String {
            return obj.description
        }*/
        return quote(obj.description)
    }


    /**
        Wrap an object, if necessary. If the object is null, return the NULL
        object. If it is an array or collection, wrap it in a JSONArray. If it is
        a map, wrap it in a JSONObject. If it is a standard property (Double,
        String, et al) then it is already wrapped. And if it doesn't, try
        to wrap it in a JSONObject. If the wrapping fails, then null is returned.
     */
    @discardableResult
    public static func wrap(_ obj: Any) -> String {
        if obj is JSONObject || obj is JSONArray || obj is Null || obj is Int || obj is Double || obj is Bool || obj is Float || obj is String || obj is Decimal {
            return (obj as! (CustomStringConvertible)).description
        } else if obj is [CustomStringConvertible] {
            return JSONArray(obj as! [CustomStringConvertible]).description
        } else if obj is Dictionary<String, CustomStringConvertible> {
            return JSONObject(obj as! Dictionary<String, CustomStringConvertible>).description
        } else if obj is UInt8 || obj is UInt16 || obj is UInt32 || obj is UInt64 || obj is Int8 || obj is Int16 || obj is Int32 || obj is Int64 {
            return (obj as! CustomStringConvertible).description
        } else if (obj is Data) {
            return (obj as! Data).base64EncodedString()
        } else if let obj = obj as? CustomStringConvertible {
            return obj.description
        }
        return JSONObject.NULL.description
    }

    /**
        Write the contents of the JSONObject as JSON text to a writer.

        For compactness, no whitespace is added.

        WARNING: This method assumes that the data structure is acyclical.
     */
    public func append(_ sb: inout Substring) throws {
        try append(&sb, 0, 0)
    }

    public static func appendValue(_ sb: inout Substring, _ obj: CustomStringConvertible?, _ indentFactor: Int, _ indent: Int) throws {
        if obj == nil {
            sb.append(contentsOf: "null")
        } else if obj is Data {
            sb.append(contentsOf: "\"\((obj as! Data).base64EncodedString())\"")
            //sb.append(contentsOf: base64ToQuotedString(obj as! Data))
        } else if obj is JSONObjectable {
            try (obj as! JSONObjectable).encoded().append(&sb, indentFactor, indent)
        } else if obj is JSONArrayable {
            try (obj as! JSONArrayable).encoded().append(&sb, indentFactor, indent)
        }else if obj is JSONObject {
            try (obj as! JSONObject).append(&sb, indentFactor, indent)
        } else if obj is JSONArray {
            try (obj as! JSONArray).append(&sb, indentFactor, indent)
        } else if obj is Dictionary<String, CustomStringConvertible> {
            try JSONObject(obj as! Dictionary<String, CustomStringConvertible>).append(&sb, indentFactor, indent)
        } else if obj is [CustomStringConvertible] {
            try JSONArray(obj as! [CustomStringConvertible]).append(&sb, indentFactor, indent)
        } else if obj is Bool {
            sb.append(contentsOf: String(obj as! Bool))
        } else if obj is Int {
            sb.append(contentsOf: try numberToString(obj as! Int))
        } else if obj is Double {
            sb.append(contentsOf: try numberToString(obj as! Double))
        } else if obj is Float {
            sb.append(contentsOf: try numberToString(obj as! Float))
        } else if obj is Decimal {
            sb.append(contentsOf: try numberToString(obj as! Decimal))
        } else if obj is UInt8 {
            sb.append(contentsOf: try numberToString(obj as! UInt8))
        } else if obj is UInt16 {
            sb.append(contentsOf: try numberToString(obj as! UInt16))
        } else if obj is UInt32 {
            sb.append(contentsOf: try numberToString(obj as! UInt32))
        } else if obj is UInt64 {
            sb.append(contentsOf: try numberToString(obj as! UInt64))
        } else if obj is Int8 {
            sb.append(contentsOf: try numberToString(obj as! Int8))
        } else if obj is Int16 {
            sb.append(contentsOf: try numberToString(obj as! Int16))
        } else if obj is Int32 {
            sb.append(contentsOf: try numberToString(obj as! Int32))
        } else if obj is Int64 {
            sb.append(contentsOf: try numberToString(obj as! Int64))
        } else { // Guranteed not to be nil here, see first predicate
            quote(obj!.description, &sb)
        }
    }

    public static func doIndent(_ sb: inout Substring, _ indent: Int) throws {
        switch indent {
        case 0:
            return
        case 4:
            sb.append(contentsOf: "    ")
            return
        case 8:
            sb.append(contentsOf: "        ");
            return
        case 12:
            sb.append(contentsOf: "            ");
            return
        case 16:
            sb.append(contentsOf: "                ");
            return
        case 20:
            sb.append(contentsOf: "                    ");
            return
        default:
            var i = 0
            while i < indent {
                sb.append(" ")
                i += i
            }
        }
    }

    /**
        Write the contents of the JSONobject as JSON text to a writer.

        For compactness, no whitespace is added.

        WARNING: This method assumes that the data structure is acyclical.
     */
    public func append(_ sb: inout Substring, _ indentFactor: Int, _ indent: Int) throws {
        do {
            var needComma : Bool = false
            let len : Int = self.count
            let keys = Array(self.map.keys)

            sb.append("{")

            if len == 1 {
                let key = keys[0]
                sb.append(contentsOf: JSONObject.quote(key))
                sb.append(":")
                if indentFactor > 0 {
                    sb.append(" ")
                }
                try JSONObject.appendValue(&sb, map[key], indentFactor, indent)
            } else if len != 0 {
                let newIndent = indent + indentFactor

                for key in keys {
                    if needComma {
                        sb.append(",")
                    }
                    if indentFactor > 0 {
                        sb.append("\n")
                    }
                    try JSONObject.doIndent(&sb, newIndent)
                    sb.append(contentsOf: JSONObject.quote(key))
                    sb.append(":")
                    if indentFactor > 0 {
                        sb.append(" ")
                    }
                    try JSONObject.appendValue(&sb, map[key], indentFactor, newIndent)
                    needComma = true
                }
                if indentFactor > 0 {
                    sb.append("\n")
                }
                try JSONObject.doIndent(&sb, indent)
            }
            sb.append("}")
        } catch {
            throw JSONError.undefined(error.localizedDescription)
        }
    }

    public var description: String {
        return toString() ?? ""
    }

    public var utf8Data: Data {
        return (toString() ?? "").data
    }
}

// MARK:- Numeric Types

extension JSONObject {

    public static func numberToString(_ number: Int) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Double) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Float) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Decimal) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = number.description

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: UInt8) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: UInt16) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: UInt32) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: UInt64) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Int8) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Int16) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Int32) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
    public static func numberToString(_ number: Int64) throws -> String {
        try JSONObject.testValidity(number)

        // Shave off trailing zeros and decimal point if possible
        var string = String(number)

        if string.contains(".") && !string.contains("e") && !string.contains("E") {
            while string.last == "0" {
                string = String(string.dropLast())
            }
            if string.last == "." {
                string = String(string.dropLast())
            }
        }
        return string
    }
}
