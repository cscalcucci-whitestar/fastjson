//
//  JSON+Exception.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 7/26/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public enum JSONError : Error {
    case undefined (String)

    public var localizedDescription: String {
        switch self {
        case .undefined(let x):
            return "JSONError - \(x)"
        @unknown default:
            return "JSONError - \(String(describing: self))"
        }
    }
}
