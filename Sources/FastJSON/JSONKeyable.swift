//
//  JSONKeyable.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 9/10/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

public protocol JSONKeyable : CustomStringConvertible {
    init?(_ string: String?)
}
