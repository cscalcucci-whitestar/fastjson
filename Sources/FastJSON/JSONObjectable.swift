//
//  JSONCodable.swift
//  WhiteStarJSON
//
//  Created by Chris Scalcucci on 9/3/20.
//  Copyright © 2020 WhiteStar. All rights reserved.
//

import Foundation
import Christy_K

public protocol JSONObjectable : JSONKeyable {
    var description : String { get }
    var utf8Data : Data { get }

    func encoded() throws -> JSONObject
    init(_ json: JSONObject) throws
    init?(_ string: String?)
    static func decoded(_ json: JSONObject) throws -> Self
}

public extension JSONObjectable {
    static func decoded(_ json: JSONObject) throws -> Self {
        return try Self.init(json)
    }

    var description : String {
        do {
            return try encoded().description
        } catch { return "Invalid Object: \(String(describing: self))" }
    }

    var utf8Data : Data {
        return description.data
    }

    init?(_ string: String?) {
        guard let string = string else { return nil }
        do {
            try self.init(try JSONObject(string))
        } catch {
            return nil
        }
    }
}

public protocol JSONArrayable : JSONKeyable {
    func encoded() throws -> JSONArray
    init(_ json: JSONArray) throws
    init?(_ string: String?)
    static func decoded(_ json: JSONArray) throws -> Self
}

public extension JSONArrayable {
    static func decoded(_ json: JSONArray) throws -> Self {
        return try Self.init(json)
    }

    var description : String {
        do {
            return try encoded().description
        } catch { return "Invalid Object: \(String(describing: self))" }
    }

    var utf8Data : Data {
        return description.data
    }

    init?(_ string: String?) {
        guard let string = string else { return nil }
        do {
            try self.init(try JSONArray(string))
        } catch {
            return nil
        }
    }
}
