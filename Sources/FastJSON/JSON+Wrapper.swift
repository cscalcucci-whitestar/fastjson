//
//  JSON+Wrapper.swift
//
//
//  Copyright © 2020 Christopher Scalcucci. All rights reserved.
//

import FastJSONWrapper

fileprivate extension StringProtocol {
    subscript(offset: Int) -> Character { self[index(startIndex, offsetBy: offset)] }
    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }

    subscript(bounds: CountableClosedRange<Int>) -> String {
        let lowerBound = Swift.max(0, bounds.lowerBound)
        guard lowerBound < self.count else { return "" }

        let upperBound = Swift.min(bounds.upperBound, self.count-1)
        guard upperBound >= 0 else { return "" }

        let i = index(startIndex, offsetBy: lowerBound)
        let j = index(i, offsetBy: upperBound-lowerBound)

        return String(self[i...j])
    }

    subscript(bounds: CountableRange<Int>) -> String {
        let lowerBound = Swift.max(0, bounds.lowerBound)
        guard lowerBound < self.count else { return "" }

        let upperBound = Swift.min(bounds.upperBound, self.count)
        guard upperBound >= 0 else { return "" }

        let i = index(startIndex, offsetBy: lowerBound)
        let j = index(i, offsetBy: upperBound-lowerBound)

        return String(self[i..<j])
    }

    func substring(_ beginIndex: Int, _ endIndex: Int) -> String {
        return self[beginIndex..<endIndex]
    }

    func substring(_ beginIndex: Int) -> String {
        return String(self.dropFirst(beginIndex))
    }
}

public enum ReaderError : Error {
    case indexOutOfBounds
    case streamClosed
}

public extension String {
    private enum StringError : Error {
        case indexOutOfBounds (String)
    }

    func getChars(_ srcBegin: Int, _ srcEnd: Int, _ dst: inout [Character?], _ dstBegin: Int) throws {

        try String.checkBoundsBeginEnd(srcBegin, srcEnd, count)
        try String.checkBoundsOffCount(dstBegin, srcEnd - srcBegin, dst.count)

        try String.getChars(self, srcBegin, srcEnd, &dst, dstBegin)
    }

    static func getChars(_ src: String, _ srcBegin: Int, _ srcEnd: Int, _ dst: inout [Character?], _ dstBegin: Int) throws {

        // Check range
        if (srcBegin < srcEnd) {
           try checkBoundsOffCount(srcBegin, srcEnd - srcBegin, src.count)
        }
        var i = srcBegin
        var x = dstBegin

        while i < srcEnd {

            dst[x] = src[i]
            x += 1
            i += 1
        }
    }

    private static func checkBoundsOffCount(_ offset: Int, _ count: Int, _ length: Int) throws {
        if (offset < 0 || count < 0 || offset > length - count) {
            throw StringError.indexOutOfBounds("offset \(offset), count \(count), length \(length)")
        }
    }

    private static func checkBoundsBeginEnd(_ begin: Int, _ end: Int, _ length: Int) throws {
        if (begin < 0 || begin > end || end > length) {
            throw StringError.indexOutOfBounds("begin \(begin), end \(end), length \(length)")
        }
    }
}



public final class FastJSONEncoder {

    public static func quote(_ str: String, _ sb: inout Substring) {
        let encoder = FastCJSONEncoderWrapper()!
        sb.append(contentsOf: encoder.quote(str))
    }
}


/**
    Takes a source string an dextracts characters and tokens from
    it. It is used by the JSONObject and JSONArray constructors to
    parse JSON source strings.
 */
public final class JSONTokener {

    public var tokener : JSONFastTokenerWrapper

    public init(_ s: String) {
        tokener = JSONFastTokenerWrapper(s)
    }

    /**
        Back up one character. This provides a sort of look-ahead capability,
        so that you can test for a digit or letter before attempting to parse
        the next number or identifier.
     */
    public func back() throws {
        tokener.back()
    }

    public func end() -> Bool {
        return tokener.end()
    }

    /**
        Get the next n characters.

        - parameter n: The number of chars to take
        - returns: String of n characters
     */
    public func next(_ n: Int) throws -> String {
        return tokener.next(Int32(n))
    }

    /**
        Get the next char in the string, skipping whitespace
     */
    public func nextClean() throws -> Character {
        let token = tokener.nextClean()
        guard let uint = UInt8(exactly: token) else {
            throw JSONError.undefined("\(token) cannot be converted to valid UInt8 ASCII representation")
        }
        return Character(UnicodeScalar(uint))
    }

    /**
        Get the next value. The value can be a Boolean, Double, Integer, JSONArray,
        JSONObject, or String, or NIL.
     */
    public func nextValue() throws -> CustomStringConvertible? {
        let retVal = tokener.nextValue()

        switch retVal {
        case "JSONObject":
            return try JSONObject(self)
        case "JSONArray":
            return try JSONArray(self)
        default:
            guard let retVal = retVal else {
                throw JSONError.undefined("Missing Value")
            }
            if retVal.contains("JSONObject+") {
                return JSONObject.stringToValue(String(retVal.dropFirst(11)))
            } else {
                return retVal
            }
        }

    }

}


//    /**
//        Get the next character in the source string.
//
//        - returns: The next character, or 0 if past the end of the source string.
//     */
//    public func next() throws -> Character {
//        return tokener.next()
//    }
//
//    /**
//        Get the next character in the source string.
//
//        - returns: The next character, or 0 if past the end of the source string.
//     */
//    public func nextFast() throws -> Character {
//        return tokener.next()
//    }
//    /**
//        Return the characters upt ot eh next closed quote character. Backslash
//        processing is dine. The formal JSON format does not allow strings in single
//        quotes, but an implementation is allowed to accept them.
//     */
//    public func nextString(_ quote: Character) throws -> String {
//        return tokener.nextString(quote)
//    }

