//
//  JSONArray.swift
//  WSDemoApp
//
//  Created by Chris Scalcucci on 7/27/20.
//  Copyright © 2020 Society. All rights reserved.
//

import Foundation

extension Sequence {
  func reduceInout<A>(_ initial: A, combine: (inout A, Iterator.Element) -> ()) -> A {
    var result = initial
    for element in self {
      combine(&result, element)
    }
    return result
  }
}

public final class JSONArray :  Sequence, IteratorProtocol, CustomStringConvertible {

    /// The underlying array where properties are kept
    private var arr : [CustomStringConvertible] = []

    // MARK:- Initialization

    public init() {}

    /**
        Construct a JSONArray from a JSONTokener.

        - parameter x: Tokener
     */
    public init(_ x: JSONTokener) throws {
        if try x.nextClean() != "[" {
            throw JSONError.undefined("A JSONArray text must start with '['")
        }
        if try x.nextClean() != "]" {
            try x.back()

            while true {
                if try x.nextClean() == "," {
                    try x.back()
                    self.arr.append(JSONObject.NULL)
                } else {
                    try x.back()
                    guard let next = try x.nextValue() else {
                        throw JSONError.undefined("Value cannot be NULL")
                    }
                    arr.append(next)
                }

                switch try x.nextClean() {
                case ",":
                    if try x.nextClean() == "]" {
                        return
                    }
                    try x.back()
                case "]":
                    return
                default:
                    throw JSONError.undefined("Expected a ',' or ']'")
                }
            }
        }
    }

    /**
        Construct a JSONArray from a source JSON text.

        - parameter src: The source string that begins with '['
            and ends with ']'
     */
    public convenience init(_ src: String) throws {
        try self.init(JSONTokener(src))
    }

    /**
        Construct a JSONArray from a source JSON utf8 byte array.

        - parameter src: The source byte array that begins with '['
            and ends with ']'
     */
    public convenience init(_ src: Data) throws {
        guard let utf8 = src.utf8String() else {
            throw JSONError.undefined("JSONArray.init(data) was not a valid utf8 representation")
        }
        try self.init(JSONTokener(utf8))
    }

    /**
        Construct a JSONArray from a colleciton.

        - parameter c: The collection to construct from
     */
    public init<T: Sequence>(_ c: T) {
        self.arr = c.compactMap({ JSONObject.wrap($0) })
    }

    public init<T: CustomStringConvertible>(_ c: Set<T>) {
        self.arr = c.map({ $0.description })
    }

    // MARK:- Properties

    public var count : Int {
        return arr.count
    }

    // MARK:- Accessors

    /**
        Get the object value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func get(_ idx: Int) throws -> CustomStringConvertible {
        guard let obj = opt(idx) else {
            throw JSONError.undefined("JSONArray[\(idx)] not found.")
        }
        return obj
    }

    /**
        Get the boolean value associated witn an index.

        The string values "true" and "false" are converted to boolean.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func getBool(_ idx: Int) throws -> Bool {
        guard let bool = Bool(try get(idx).description) else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a boolean.")
        }
        return bool
    }

    /**
        Get the double value associated with an index.

        - paramter idx: The index must be between 0 and count - 1
     */
    public func getDouble(_ idx: Int) throws -> Double {
        guard let double = Double(try get(idx).description) else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a double.")
        }
        return double
    }

    /**
        Get the Decimal value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func getDecimal(_ idx: Int) throws -> Decimal {
        guard let dec = Decimal(string: try get(idx).description) else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a decimal.")
        }
        return dec
    }

    /**
        Get the Int value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func getInt(_ idx: Int) throws -> Int {
        guard let int = Int(try get(idx).description) else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a number.")
        }
        return int
    }

    /**
        Get the JSONArray associated with an index.

        - paramter idx: The index must be between 0 and count - 1
     */
    public func getJSONArray(_ idx: Int) throws -> JSONArray {
        guard let obj = try get(idx) as? JSONArray else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a JSONArray.")
        }
        return obj
    }

    /**
        Get the JSONObject associated with an index.

        - paramter idx: The index must be between 0 and count - 1
     */
    public func getJSONObject(_ idx: Int) throws -> JSONObject {
        guard let obj = try get(idx) as? JSONObject else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a JSONObject.")
        }
        return obj
    }

    /**
        Get the enum value associated with an index.

        - parameter type: The enum class expected to be returned
        - parameter idx: The index must be between 0 and count - 1
     */
    public func getEnum<T: RawRepresentable>(_ type: T.Type, at idx: Int) throws -> T {
        guard let obj = optEnum(type, idx) else {
            throw JSONError.undefined("JSONArray[\(idx)] is not an enum of type \(type).")
        }
        return obj
    }

    /**
        Get the bytes back associated with a key.
     */
    public func getByteArray(_ idx: Int) throws -> Data {
        guard let data = optByteArray(idx) else {
            throw JSONError.undefined("JSONArray[\(idx)] is not a byte array.")
        }
        return data
    }

    /**
        Get the Int value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func getString(_ idx: Int) throws -> String {
        return try get(idx).description
    }

    /**
        Determine if the value is null.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func isNull(_ idx: Int) -> Bool {
        return opt(idx) == JSONObject.NULL
    }

    /**
        Make a string from the contents of this JSONArray.

        The separator string is inserted between each element.
        WARNING: This method assumes the data structure is acyclical.

        - parameter sep: A string that will be inserted between elements.
     */
    public func join(_ sep: String) throws -> String {
        var first = false

        return String(self.reduceInout(Substring()) { (res: inout Substring, e) in
            if !first {
                res.append(contentsOf: sep)
            } else { first = false }

            do {
                if let str = try JSONObject.valueToString(e) {
                    res.append(contentsOf: str)
                }
            } catch {
                // Do nothing
            }
        })
    }

    /**
        Get the optional object value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func opt(_ idx: Int) -> CustomStringConvertible? {
        return (idx < 0 || idx >= count) ? nil : arr[idx]
    }

    /**
        Get the optional boolean value associated with an index.

        It returns false if there is no value at that index, or if
        the value is not boolean.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func optBool(_ idx: Int) -> Bool {
        return optBool(idx, false)
    }

    /**
        Get the optional boolean value associated with an index.

        It returns the default if there is no value at that index
        or if it is not a boolean.

        - parameter idx: The index must be between 0 and count - 1
        - parameter default: A boolean default
     */
    public func optBool(_ idx: Int, _ default: Bool) -> Bool {
        do {
            return try getBool(idx)
        } catch { return `default` }
    }

    /**
        Get the optional Double value associated with an index. NaN is
        returned if there is no value for the index, or if the value is
        not a number and cannot be converted into one.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func optDouble(_ idx: Int) -> Double {
        return optDouble(idx, Double.nan)
    }

    /**
        Get the Double value associated with an index.

        The default is returned if there is no proper value for
        the index.

        - parameter idx: The subscript
        - parameter default: A Double default
     */
    public func optDouble(_ idx: Int, _ default: Double) -> Double {
        do {
            return try getDouble(idx)
        } catch { return `default` }
    }

    /**
        Get the optional Int value associated with an index.

        Zero is returned if there is no proper Int value.

        - parameter idx: The index must be between 0 and count - 1
        - parameter default: The default Int to return
     */
    public func optInt(_ idx: Int) -> Int {
        return optInt(idx, 0)
    }

    /**
        Get the Int value associated with an index.

        The default is returned if there is no proper value for
        the index.

        - parameter idx: The subscript
        - parameter default: An Int default
     */
    public func optInt(_ idx: Int, _ default: Int) -> Int {
        do {
            return try getInt(idx)
        } catch { return `default` }
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The expected type to be returned
        - parameter idx: The index must be between 0 and count - 1
     */
    public func optEnum<T: RawRepresentable>(_ type: T.Type, _ idx: Int) -> T? {
        return optEnum(type, idx, nil)
    }

    /**
        Get the enum value associated with a key.

        - parameter type: The expected type to be returned
        - parameter idx: The index must be between 0 and count - 1
        - parameter default: The default value to be returned
     */
    public func optEnum<T: RawRepresentable>(_ type: T.Type, _ idx: Int, _ default: T?) -> T? {
        guard let obj = opt(idx) as? T.RawValue, let t = T(rawValue: obj) else {
            return `default`
        }
        return t
    }

    /**
        Get the optional Decimal value associated with an index.

        The default is returned if there is no proper Decimal value found.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func optDecimal(_ idx: Int) -> Decimal? {
        return optDecimal(idx, nil)
    }

    /**
        Get the optional Decimal value associated with an index.

        The default is returned if there is no proper Decimal value found.

        - parameter idx: The index must be between 0 and count - 1
        - parameter default: The default value to return
     */
    public func optDecimal(_ idx: Int, _ default: Decimal?) -> Decimal? {
        do {
            return try getDecimal(idx)
        } catch { return `default` }
    }

    /**
        Get the optional JSONArray value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func optJSONArray(_ idx: Int) -> JSONArray? {
        return opt(idx) as? JSONArray
    }

    /**
        Get the optional JSONObject value associated with an index.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func optJSONObject(_ idx: Int) -> JSONObject? {
        return opt(idx) as? JSONObject
    }

    /**
        Get the optional String value associated with an index.

        The default is returned if there is no proper String value found.

        - parameter idx: The index must be between 0 and count - 1
     */
    public func optString(_ idx: Int) -> String? {
        return optString(idx, "")
    }

    /**
        Get the optional JSONObject value associated with an index.

        The default is returned if there is no proper JSONObject value found.

        - parameter idx: The index must be between 0 and count - 1
        - parameter default: The default value to return
     */
    public func optString(_ idx: Int, _ default: String?) -> String? {
        return (opt(idx) as? String) ?? `default`
    }

    /**
        Get the optional JSONObject value associated with an index.

        The default is returned if there is no proper JSONObject value found.

        - parameter idx: The index must be between 0 and count - 1
        - parameter default: The default value to return
     */
    public func optByteArray(_ idx: Int) -> Data? {
        return optByteArray(idx, nil)
    }

    /**
        Get the optional JSONObject value associated with an index.

        The default is returned if there is no proper JSONObject value found.

        - parameter idx: The index must be between 0 and count - 1
        - parameter default: The default value to return
     */
    public func optByteArray(_ idx: Int, _ default: Data?) -> Data? {
        return (opt(idx) as? String)?.base64Decoded ?? `default`
    }

    // MARK:- Insert

    /**
        Append a string value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put(_ obj: String) -> JSONArray {
        self.arr.append(obj)
        return self
    }

    /**
        Append a boolean value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put(_ obj: Data) -> JSONArray {
        self.arr.append(obj)
        return self
    }

    /**
        Append a boolean value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put(_ obj: JSONObject) -> JSONArray {
        self.arr.append(obj)
        return self
    }

    /**
        Append a boolean value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put(_ obj: JSONArray) -> JSONArray {
        self.arr.append(obj)
        return self
    }

    /**
        Append a boolean value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put<T: JSONObjectable>(_ obj: T) throws -> JSONArray {
        self.arr.append(try obj.encoded())
        return self
    }

    /**
        Append a boolean value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put<T: JSONArrayable>(_ obj: T) throws -> JSONArray {
        self.arr.append(try obj.encoded())
        return self
    }


    /**
        Append a boolean value. This increases the array's length by one.

        - parameter obj: The boolean to be inserted
     */
    @discardableResult
    public func put(_ obj: Bool) -> JSONArray {
        self.arr.append(obj)
        return self
    }

    /**
        Put a JSONArray in the current JSONArray constructed from a sequence.

        - parameter seq: The sequence to place
     */
    @discardableResult
    public func put<T: Sequence>(_ seq: T) -> JSONArray {
        self.arr.append(JSONArray(seq))
        return self
    }

    /**
        Put a JSONArray in the current JSONArray constructed from a sequence.

        - parameter seq: The sequence to place
     */
    @discardableResult
    public func put<T: JSONObjectable>(_ seq: [T]) throws -> JSONArray {
        try seq.forEach({
            self.arr.append(try $0.encoded())
        })
        return self
    }

    /**
        Append a double value. This increases the array's length by one.

        Throws if the value is not finite

        - parameter obj: The double to be inserted
     */
    @discardableResult
    public func put(_ obj: Double) throws -> JSONArray {
        try JSONObject.testValidity(obj)
        self.arr.append(obj)
        return self
    }

    /**
        Append an Int value. This increases the array's length by one.

        - parameter obj: The Int to be inserted
     */
    @discardableResult
    public func put(_ obj: Int) -> JSONArray {
        self.arr.append(obj)
        return self
    }

    /**
        Put a Map in the JSONArray, where the input will be a JSONObject that
        is produced from the Map.

        - parameter obj: The Map to insert
     */
    @discardableResult
    public func put(_ map: Dictionary<String,CustomStringConvertible>) -> JSONArray {
        self.arr.append(JSONObject(map))
        return self
    }

    /**
        Put a Map in the JSONArray,
     */

    /**
        Put or replace a boolean value in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements
        will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The boolean value
     */
    @discardableResult
    public func put(_ obj: Bool, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    /**
        Put or replace a JSONArray in the JSONArray, made from the provided sequence.

        If the index is greater than the length of the JSONArray, then nil elements
        will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Sequence value
     */
    @discardableResult
    public func put(_ obj: [CustomStringConvertible], at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    /**
        Put or replace a Double in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements
        will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Double value
     */
    @discardableResult
    public func put(_ obj: Double, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    /**
        Put or replace an Int in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Int value
     */
    @discardableResult
    public func put(_ obj: Int, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    /**
        Put or replace a JSONObject in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Int value
     */
    @discardableResult
    public func put(_ obj: JSONObject, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    /**
        Put or replace a JSONArray in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Int value
     */
    @discardableResult
    public func put(_ obj: JSONArray, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    /**
        Put or replace a JSONObjectable in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Int value
     */
    @discardableResult
    public func put<T: JSONObjectable>(_ obj: T, at idx: Int) throws -> JSONArray {
        try putAndPad(try obj.encoded(), at: idx)
        return self
    }


    /**
        Put or replace a JSONArrayable in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Int value
     */
    @discardableResult
    public func put<T: JSONArrayable>(_ obj: T, at idx: Int) throws -> JSONArray {
        try putAndPad(try obj.encoded(), at: idx)
        return self
    }


    /**
        Put any JSONable object in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements will be added as necessary to pad it out.

     - parameter idx: The subscript
     - parameter obj: The value
     */
    @discardableResult
    public func putAny<T: CustomStringConvertible>(_ obj: T, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    @discardableResult
    public func putAny(_ obj: CustomStringConvertible?) throws -> JSONArray {
        guard let obj = obj else { return self }
        arr.append(obj)
        return self
    }

    /**
        Put or replace a Map in the JSONArray.

        If the index is greater than the length of the JSONArray, then nil elements
        will be added as necessary to pad it out.

        - parameter idx: The subscript
        - parameter obj: The Map value
     */
    @discardableResult
    public func put<K: CustomStringConvertible, V: CustomStringConvertible>(_ obj: Dictionary<K,V>, at idx: Int) throws -> JSONArray {
        try putAndPad(obj, at: idx)
        return self
    }

    @discardableResult
    private func putAndPad<T: CustomStringConvertible>(_ obj: T, at idx: Int) throws -> JSONArray {
        try JSONObject.testValidity(obj)

        guard idx >= 0 else {
            throw JSONError.undefined("\(idx) is not a valid index.")
        }

        if idx < count {
            arr[idx] = obj
        } else {
            while (idx != count) {
                arr.append(JSONObject.NULL)
            }
            arr.append(obj)
        }
        return self
    }

    // MARK:- Removal

    /**
        Remove an index and close the hole.

        - parameter idx: The index of the element to be removed
        - returns: The value that was associated with the index, or nil
     */
    @discardableResult
    public func remove(_ idx: Int) -> CustomStringConvertible? {
        return (idx >= 0 && idx < count) ? arr.remove(at: idx) : nil
    }

    // MARK:- Inspection

    /**
        Determines if two JSONArrays are similar.

        They must contain similar sequences.

        - parameter oth: The other JSONArray
     */
    public func similar(_ oth: JSONArray) -> Bool {
        guard count == oth.count else {
            return false
        }
        var i = 0

        do {
            while i < count {
                let this = try get(i)
                let that = try oth.get(i)

                if let thisOBJ = this as? JSONObject, let thatOBJ = that as? JSONObject {
                    if !thisOBJ.similar(thatOBJ) {
                        return false
                    }
                } else if let thisARR = this as? JSONArray, let thatARR = that as? JSONArray {
                    if !thisARR.similar(thatARR) {
                        return false
                    }
                } else if type(of: this) != type(of: that) {
                    return false
                } else if this.description != that.description {
                    return false
                }
                i += 1
            }
        } catch {
            return false
        }
        return true
    }

    /**
        Produce a JSONObject by combining a JSONArray of keys with the values
        of this JSONArray.

        - parameter keys: JSONArray contianing a list of key strings
        - returns: A JSONObject, or nil if there are no keys or if this JSONArray
            has no values
     */
    public func toJSONObject(_ keys: JSONArray) throws -> JSONObject? {
        guard keys.count != 0 && count != 0 else { return nil }

        let jo = JSONObject()
        var i = 0

        do {
            while i < keys.count {
                try jo.put(try keys.getString(i), opt(i))
                i += 1
            }
        } catch {
            return nil
        }
        return jo
    }

    // MARK:- String Convertible

    public var description: String {
        return self.toString() ?? "Unable to produce valid string"
    }

    public var utf8Data: Data {
        return (toString() ?? "").data
    }

    /**
        Make a JSON text of this JSONArray. For compactness, no unnecessary
        whitespace is added.

        If it is not possible to produce a syntactically correct JSON text then
        nil will be returned. This could occur if the array contains an invalid number.

        WARNING: This method assumes the data structure is acyclical.

        - returns: A printable, displayable, transmittable representation of the
        array.
     */
    public func toString() -> String? {
        do {
            return try toString(0)
        } catch {
            return nil
        }
    }

    /**
        Make a pretty printed JSON text of this JSONArray.

        WARNING: This method assumes that the data structure is acyclical.
     */
    public func toString(_ indentFactor: Int) throws -> String {
        var sb = Substring()
        try append(&sb, indentFactor, 0)
        return String(sb)
    }

    /**
        Write the contents of the JSONArray as JSON text to a Substring. For
        compactness, no whitespace is added.

        WARNING: This method assumes that the data structure is acyclical.

        - parameter sb: The Substring
     */
    public func append(_ sb: inout Substring) throws {
        try append(&sb, 0,0)
    }

    /**
        Write the contents of the JSONArray as JSON text to the Substring.
        For compactness, no whitespace is added.

        WARNING: This method assumes that the data structure is acyclical.

        - parameter sb: The substring to attach to
        - parameter indentFactor: The number of spaces to add to each level
     */
    public func append(_ sb: inout Substring, _ indentFactor: Int, _ indent: Int) throws {
        do {
            var commanate = false
            let length : Int = count
            sb.append("[")

            if length == 1 {
                try JSONObject.appendValue(&sb, arr[0], indentFactor, indent)
            } else if length != 0 {
                let newIndent = indent + indentFactor

                var i = 0

                while i < length {
                    if commanate {
                        sb.append(",")
                    }
                    if indentFactor > 0 {
                        sb.append("\n")
                    }
                    try JSONObject.doIndent(&sb, newIndent)
                    try JSONObject.appendValue(&sb, arr[i], indentFactor, newIndent)
                    commanate = true

                    i += 1
                }
                if indentFactor > 0 {
                    sb.append("\n")
                }
                try JSONObject.doIndent(&sb, indent)
            }
            sb.append("]")
        } catch {
            throw JSONError.undefined(error.localizedDescription)
        }
    }

    // MARK:- High Order Lambdas

    public func objectMap() -> [JSONObject] {
        return self.compactMap({ $0 as? JSONObject })
    }

    public func mapObjects<T>(_ fn: (JSONObject) -> T?) -> [T?] {
        self.map({
            guard let obj = $0 as? JSONObject else { return nil }
            return fn(obj)
        })
    }

    public func compactMapObjects<T>(_ fn: (JSONObject) throws -> T?) -> [T] {
        self.compactMap({
            do {
                guard let obj = $0 as? JSONObject else { return nil }
                return try fn(obj)
            } catch {
                return nil
            }
        })
    }

    public func forEachObject(_ fn: (JSONObject) -> ()) {
        self.forEach({
            guard let obj = $0 as? JSONObject else { return }
            fn(obj)
        })
    }


    // MARK:- Iterator
    public typealias Element = CustomStringConvertible

    private var curPos = 0

    public func makeIterator() -> JSONArray {
        curPos = 0
        return self
    }

    public func next() -> Element? {
        if curPos < arr.count {
            let oldPos = curPos
            curPos += 1
            return arr[oldPos]
        }
        return nil
    }

}
